//
//  ImageGalleryContentViewController.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 04/03/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import UIKit

class ImageGalleryContentViewController: UIViewController, UIScrollViewDelegate {
    
    let товар: Товар?
    
    let index: Int
    
    let imageView = UIImageView()
    
    let scrollView = StretchableScrollView()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        self.товар = nil
        self.index = 0
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        self.index = 0
        self.товар = nil
        
        super.init(coder: aDecoder)
    }
    
    init(товар: Товар, index: Int) {
        
        self.товар = товар
        self.index = index
        
        if index < товар.images.count {
            imageView.image = товар.images[index]
            imageView.backgroundColor = .whiteColor()
        } else {
            imageView.backgroundColor = UIColor(white: 1 - CGFloat(index) / 20, alpha: 1)
        }
        
        super.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        scrollView.delegate = self
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.width)
        
        imageView.contentMode = .ScaleAspectFill
        imageView.frame = scrollView.frame
        imageView.clipsToBounds = true
        
        scrollView.addSubview(imageView)
        
        view = scrollView
        
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 1.5
    }
    
    
    override func viewDidDisappear(animated: Bool) {
        
        guard imageView.image == nil else { return }
            if index < товар?.images.count {
                imageView.image = товар?.images[index]
                imageView.backgroundColor = .whiteColor()
            }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: Scroll view delegate
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageView
    }

}


class StretchableScrollView: UIScrollView {
    
    override var frame: CGRect {
        
        didSet {
            contentSize = frame.size
            
            if frame.height == UIScreen.mainScreen().bounds.height {
                maximumZoomScale = 1.5
                subviews.forEach { ($0 as? UIImageView)?.contentMode = .ScaleAspectFit }
            } else if maximumZoomScale != 1 {
                maximumZoomScale = 1
                subviews.forEach { ($0 as? UIImageView)?.contentMode = .ScaleAspectFill }
            }
            
            subviews.forEach { $0.frame = bounds }
        }
    }
    
}
