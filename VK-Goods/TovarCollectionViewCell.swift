//
//  TovarCollectionViewCell.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/29/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import UIKit

class TovarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var likeImageView: UIImageView! {
        didSet {
            likeImageView.tintColor = Constants.mainColor
        }
    }
    
    var likeIt = false {
        didSet {
            likeImageView.image = likeIt ? UIImage(named: "LikeOpaqueSelected") : UIImage(named: "LikeOpaque")
            
            NSNotificationCenter.defaultCenter().postNotificationName("likeButtonInCellHasBeenTapped", object: self, userInfo: ["likeIt": likeIt])
        }
    }
    
    
    @IBAction func likeButtonTap() {
        
        likeIt = !likeIt
    }
    
}
