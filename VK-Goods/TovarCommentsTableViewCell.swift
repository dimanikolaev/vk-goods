//
//  TovarCommentsTableViewCell.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/27/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit

class TovarCommentsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var backView: UIView! {
        didSet {
            backView.backgroundColor = UIColor(white: 0.95, alpha: 1)
            backView.layer.cornerRadius = 64
        }
    }

    @IBOutlet weak var tovarImageView: UIImageView! {
        didSet {
            tovarImageView.layer.cornerRadius = 60
        }
    }
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var tovarTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.transform = CGAffineTransformMakeScale(1, -1)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
