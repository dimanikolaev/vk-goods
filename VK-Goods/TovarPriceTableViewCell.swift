//
//  TovarPriceTableViewCell.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 3/12/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import UIKit

class TovarPriceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var priceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
