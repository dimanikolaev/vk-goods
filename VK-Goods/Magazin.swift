//
//  Magazin.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 3/11/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

typealias Magazin = Магазин

class Магазин: Equatable {
    
    let id: Int
    let contactId: Int
    let title: String
    var photo: UIImage?
    let currencyId: Int
    
    init?(dic: Dictionary<String, AnyObject>) {
        
        guard let market = (dic["market"] as? Dictionary<String, AnyObject>) where market["enabled"] as? Bool == true,
              let   id = dic["id"] as? Int,
                    contactId = market["contact_id"] as? Int,
                    title = dic["name"] as? String,
                    currencyId = (market["currency"] as? Dictionary<String, AnyObject>)?["id"] as? Int else {
            self.id = 0
            self.contactId = 0
            self.title = ""
            self.photo = nil
            self.currencyId = 0
            return nil
        }
                
        self.id = id
        self.contactId = contactId
        self.title = title
        self.photo = nil
        self.currencyId = currencyId
    }
    
}

func ==(lhs: Магазин, rhs: Магазин) -> Bool {
    return lhs.id == rhs.id
}