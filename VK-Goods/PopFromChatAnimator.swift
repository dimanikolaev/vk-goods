//
//  PopFromChatAnimator.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/26/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

class PopFromChatAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var transitionShouldBeCompleted = false
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return Constants.TransitionDuration
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        defer {
            if !transitionShouldBeCompleted {
                print("defer")
                transitionContext.completeTransition(false)
            }
        }
        
        guard   let containerView = transitionContext.containerView(),
                    fromView = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)?.view,
                    toView = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)?.view,
                    toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey),
                    tabBar = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)?.tabBarController?.tabBar else { return }
        
        transitionShouldBeCompleted = true
        
        toView.frame = CGRect(origin: CGPoint(x: toView.frame.width / -3, y: 0), size: toView.frame.size)
        
        containerView.insertSubview(toView, belowSubview: fromView)
        
        fromView.clipsToBounds = false
        
        fromView.layer.shadowPath = UIBezierPath(rect: CGRect(origin: CGPoint(x: 16, y: 0), size: fromView.bounds.size)).CGPath
        
        fromView.layer.rasterizationScale = UIScreen.mainScreen().scale
        fromView.layer.shouldRasterize = true
        
        fromView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        fromView.layer.shadowOpacity = 1
        fromView.layer.shadowRadius = 16
        
        let duration = transitionDuration(transitionContext)
        
        UIView.animateWithDuration(duration, delay: 0, options: Constants.TransitionOptions, animations: { () -> Void in
            fromView.frame = CGRect(origin: CGPoint(x: fromView.frame.width, y: 0), size: fromView.frame.size)
            
            toView.frame = CGRect(origin: CGPoint.zero, size: toView.frame.size)
            
            tabBar.frame = CGRect(origin: CGPoint(x: 0, y: fromView.frame.height - tabBar.frame.height), size: tabBar.frame.size)

            }, completion: { (_) -> Void in
                toViewController.tabBarController?.view.addSubview(tabBar)
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        })
    }
    
    
}
