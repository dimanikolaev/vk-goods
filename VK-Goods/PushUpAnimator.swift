//
//  PushUpAnimator.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/29/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//


class PushUpAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var transitionShouldBeCompleted = false
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return Constants.TransitionDuration
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        defer {
            if !transitionShouldBeCompleted {
                transitionContext.completeTransition(false)
            }
        }
        
        guard let   containerView = transitionContext.containerView(),
                    fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? PodborkasTableViewControllerProtocol,
                    toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as? PodborkaCollectionViewController,
                    toView = toViewController.view else { return }
        
        transitionShouldBeCompleted = true
        
        let duration = transitionDuration(transitionContext)
        
        toView.frame = CGRect(origin: fromViewController.selectedCellFrame.origin, size: toView.frame.size)
        
        let maskRect = CGRect(origin: CGPoint(x: 0, y: 64 + Constants.PodborkaCellHeight), size: CGSize(width: toView.frame.width, height: toView.frame.height))
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(rect: maskRect).CGPath
        
        toView.layer.mask = maskLayer
        toView.alpha = 0.25
        
        containerView.addSubview(toView)
        
        var imageView: UIImageView?
        
        var titleView: UIView?
        var titleLabel: UILabel?
        
        if let cellTitleView = fromViewController.selectedCell?.titleView, cellTitleLabel = fromViewController.selectedCell?.titleLabel {
            
            imageView = UIImageView(image: toViewController.подборка?.coverImage ?? UIImage(named: "Marketplace"))
            imageView?.frame = CGRect(origin: CGPoint(x: 0, y: fromViewController.selectedCellFrame.origin.y + 64), size: fromViewController.selectedCellFrame.size)
            imageView?.contentMode = .ScaleAspectFill
            imageView?.clipsToBounds = true
            containerView.addSubview(imageView!)
            
            titleView = UIView(frame: cellTitleView.frame)
            titleView?.backgroundColor = cellTitleView.backgroundColor
            
            titleLabel = UILabel(frame: cellTitleLabel.frame)
            titleLabel?.text = cellTitleLabel.text
            titleLabel?.font = cellTitleLabel.font
            titleLabel?.textColor = cellTitleLabel.textColor
            titleLabel?.backgroundColor = titleView!.backgroundColor
            
            titleView?.addSubview(titleLabel!)
            imageView?.addSubview(titleView!)
        }
        
        UIView.animateWithDuration(duration, delay: 0, options: Constants.TransitionOptions, animations: { () -> Void in
            toView.alpha = 1
            titleView?.alpha = 0
            imageView?.frame = CGRect(origin: CGPoint(x: 0, y: 64), size: imageView!.frame.size)
            toView.frame = CGRect(origin: CGPoint.zero, size: toView.frame.size)
            fromViewController.tableView.alpha = 0
            }, completion: { (_) -> Void in
                imageView?.removeFromSuperview()
                toView.layer.mask = nil
                fromViewController.tableView.alpha = 1
                fromViewController.selectedCell?.hidden = false
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        })
        
    }
    
}
