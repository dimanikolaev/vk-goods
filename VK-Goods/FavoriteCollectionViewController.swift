//
//  FavoriteCollectionViewController.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/20/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class FavoriteCollectionViewController: UICollectionViewController, TovarsCollectionViewControllerProtocol, VKGoodsTovarnyDelegate {

    let goods = (UIApplication.sharedApplication().delegate as? AppDelegate)?.goods
    
    var товары = Array<Товар>()
    
    var activityIndicatorView: UIActivityIndicatorView?
    
    var loadingTovars = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Register cell classes
        collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        // Do any additional setup after loading the view.
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateCoverImageOfTovarFromNotification:", name: "tovarHasBeenUpdated", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateTovarLikeInCellFromNotification:", name: "likeButtonInCellHasBeenTapped", object: nil)
        
        collectionView?.alwaysBounceVertical = true
        
        let width = view.frame.width / 2.2
        tovarCellSize = CGSize(width: width, height: width + 112)
        
        let inset = (view.frame.width - width * 2) / 3.333333
        tovarsSectionEdgeInsets = UIEdgeInsets(top: 10, left: inset, bottom: 0, right: inset)
        
        activityIndicatorView = UIActivityIndicatorView(frame: view.frame)
        activityIndicatorView?.activityIndicatorViewStyle = .Gray
        view.addSubview(activityIndicatorView!)
        activityIndicatorView?.startAnimating()
        
        goods?.favoritesDelegate = self
    }
    
    
    override func viewDidAppear(animated: Bool) {
        if let navigationBarFrame = navigationController?.navigationBar.frame {
            selectedCellThumbPhotoTopIndent = navigationBarFrame.origin.y + navigationBarFrame.height
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return товары.count
    }
    
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let товар = товары[indexPath.row]
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Tovar Cell", forIndexPath: indexPath) as! TovarCollectionViewCell
        cell.thumbImageView.image = товар.thumbPhoto
        cell.titleLabel.text = товар.title
        cell.priceLabel.text = товар.priceText
        cell.likeIt = товар.likeIt
        return cell
    }
    
    
    override func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) { () -> Void in
            if self.товары.count == self.arrayMaxCount {
                if indexPath.row == self.товары.count - 1 {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.uploadMoreTovars()
                    }
                }
            }
        }
    }
    
    
    // MARK: UICollectionViewDelegate
    
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        guard indexPath.row < товары.count else { return }
        
        selectedCell = collectionView.cellForItemAtIndexPath(indexPath) as? TovarCollectionViewCell
        
        guard let cellRect = collectionView.layoutAttributesForItemAtIndexPath(indexPath)?.frame else { return }
        
        guard selectedCell != nil else { return }
        
        selectedCellThumbPhotoFrame = CGRect(origin: CGPoint(x: cellRect.origin.x + selectedCell!.thumbImageView.frame.origin.x, y: cellRect.origin.y + selectedCell!.thumbImageView.frame.origin.y  - collectionView.contentOffset.y), size: selectedCell!.thumbImageView.frame.size)
        
        performSegueWithIdentifier("Show Tovar", sender: товары[indexPath.row])
    }
  
    
    // MARK: UICollectionViewFlowLayoutDelegate
    
    
    lazy var tovarCellSize = CGSize(width: 300, height: 300)
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    
        return tovarCellSize
    }
    
    
    lazy var tovarsSectionEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return tovarsSectionEdgeInsets
    }
    
    
    // MARK: - TovarsCollectionViewController protocol
    
    
    var selectedCellThumbPhotoTopIndent: CGFloat = 0
    var selectedCellThumbPhotoFrame = CGRect.zero
    var selectedCell: TovarCollectionViewCell?
    
    
    // MARK: - Товары
    
    
    var currentOffset = 0
    
    let quantityPerOneRequest = Constants.QuantityPerOneRequest
    
    var arrayMaxCount = Constants.QuantityPerOneRequest
    
    
    func uploadMoreTovars() {
        
        loadingTovars = true
        
        print("uploadTovar")
        
        currentOffset += quantityPerOneRequest
        
        arrayMaxCount += quantityPerOneRequest
        
        goods?.getFavoriteTovarsUseOffset(currentOffset, withMaxCount: quantityPerOneRequest) { (товары, error) -> Void in
            self.loadingTovars = false
            guard error == nil else {
                print(error)
                return
            }
        }
    }

    
    // MARK: - VKGoods tovary delegate
    
    
    var shouldLoad = true
    
    weak var viewController: UIViewController?
    
    
    func addNewTovar(товар: Товар) {
        
        if activityIndicatorView != nil {
            
            activityIndicatorView?.removeFromSuperview()
            activityIndicatorView = nil
        }
        
        товары.append(товар)
        
        let indexPath = NSIndexPath(forRow: товары.count - 1, inSection: 0)
        collectionView?.insertItemsAtIndexPaths([indexPath])
    }
    
    
    // MARK: - Navigation
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        guard segue.identifier == "Show Tovar" else { return }
        
        if let товар = sender as? Товар {
            goods?.loadPhotosOfTovar(товар)
            (segue.destinationViewController as? TovarTableViewController)?.товар = товар
        }
    }

    
    // MARK: - Notifications
    
    
    func updateCoverImageOfTovarFromNotification(notification: NSNotification) {
        
        guard   let товар = notification.object as? Товар,
                index = товары.indexOf({ $0 == товар }),
                cell = collectionView?.cellForItemAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as? TovarCollectionViewCell
                else { return }
        
        UIView.transitionWithView(cell.thumbImageView, duration: 0.25, options: .TransitionCrossDissolve, animations: { () -> Void in
            cell.thumbImageView.image = товар.thumbPhoto
            }, completion: nil)
    }
    
    
    func updateTovarLikeInCellFromNotification(notification: NSNotification) {
        
        guard   let cell = notification.object as? TovarCollectionViewCell,
                let indexPath = collectionView?.indexPathForCell(cell) where indexPath.row < товары.count,
                let likeIt = notification.userInfo?["likeIt"] as? Bool else { return }
        
        товары[indexPath.row].likeIt = likeIt
    }
}
