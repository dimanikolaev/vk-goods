//
//  TextMessageTableViewCell.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 2/21/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit

class TextMessageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var speechBubbleView: SpeechBubbleView!
    
    @IBOutlet weak var messageLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        transform = CGAffineTransformMakeScale(1, -1)
        backgroundColor = Constants.ChatBackgroundColor
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setSpeechBubbleSideTo(side: MessageSide) {
        speechBubbleView.side = side
        
        switch speechBubbleView.side {
        case .Left:
            speechBubbleView.backgroundColor = .whiteColor()
            messageLabel.textColor = .blackColor()
        case .Right:
            speechBubbleView.backgroundColor = UIColor(white: 0.87, alpha: 1)
            messageLabel.textColor = .darkGrayColor()
        }
    }
    
}

class SpeechBubbleView: UIView {
    
    var side = MessageSide.Left {
        didSet {
            layer.cornerRadius = Constants.SpeechBubbleCornersRadius
            
            switch side {
            case .Left:
                backgroundColor = UIColor(white: 0.95, alpha: 1)
            case .Right:
                backgroundColor = Constants.mainColor
            }
        }
    }
}

enum MessageSide {
    case Left, Right
}