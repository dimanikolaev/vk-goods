//
//  ChatViewController.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 2/20/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.contentInset = UIEdgeInsets(top: -64, left: 0, bottom: 0, right: 0)
            tableView.contentOffset = CGPoint.zero
            // Мне нравится вот-так переворачивать таблицу для чатов
            tableView.transform = CGAffineTransformMakeScale(1, -1)
            tableView.estimatedRowHeight = 64
            tableView.backgroundColor = Constants.ChatBackgroundColor
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = tableView.backgroundColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Right Text Message Cell", forIndexPath: indexPath) as! TextMessageTableViewCell
        cell.setSpeechBubbleSideTo(.Right)
        cell.messageLabel.text = "Hello, can you hear me? I'm in California dreaming about who we used to be"
        return cell
    }
    
    // MARK: - Table view delegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
