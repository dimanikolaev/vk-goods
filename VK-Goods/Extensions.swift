//
//  Extensions.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 2/20/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

extension String {
    
    /**
     
     An localized string with no comments.
     
     */
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: NSBundle.mainBundle(), value: "", comment: "")
    }
    
    /**
     
     Returns an localized string with comment.
     - returns: String
     - parameter comment: String
     
     */
    
    func localizedWithComment(comment: String) -> String {
        return NSLocalizedString(self, tableName: nil, bundle: NSBundle.mainBundle(), value: "", comment: comment)
    }
    
}

extension UIView {
    
    /**
     
     Rounds the corners of UIView.
     - parameter corners: `[.TopLeft, .TopRight, .BottomLeft, .BottomRight, .AllCorners]` A bitmask value that identifies the corners that you want rounded.
     - parameter radius: The radius of each corner oval. Values larger than half the rectangle’s width or height are clamped appropriately to half the width or height.
     
     */
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let mask = CAShapeLayer()
        mask.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).CGPath
        self.layer.mask = mask
    }
    
}

extension UIImage {
    
    var circleImage: UIImage? {
        let square = CGSize(width: min(size.width, size.height), height: min(size.width, size.height))
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        imageView.contentMode = .ScaleAspectFill
        imageView.image = self
        imageView.layer.cornerRadius = square.width/2
        imageView.layer.masksToBounds = true
        UIGraphicsBeginImageContext(imageView.bounds.size)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.renderInContext(context)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
}
