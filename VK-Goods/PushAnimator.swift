//
//  PushAnimator.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/29/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//


class PushAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var transitionShouldBeCompleted = false
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return Constants.TransitionDuration
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        defer {
            if !transitionShouldBeCompleted {
                print("defer")
                transitionContext.completeTransition(false)
            }
        }
        
        guard   let containerView = transitionContext.containerView(),
                fromView = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)?.view,
                toView = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)?.view
//                tabBar = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)?.tabBarController?.tabBar
                else { return }
        
        transitionShouldBeCompleted = true
        
        let duration = transitionDuration(transitionContext)
        
        toView.frame = CGRect(origin: CGPoint(x: toView.frame.width, y: 0), size: toView.frame.size)
        
        toView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        toView.layer.shadowOpacity = 1
        toView.layer.shadowRadius = 6
        
        containerView.addSubview(toView)
        
//        containerView.addSubview(tabBar)
        
        
        let animation = CABasicAnimation(keyPath: "shadowOffset.width")
        animation.fromValue = -1
        animation.toValue = 6
        animation.repeatCount = 1
        animation.duration = duration
        toView.layer.addAnimation(animation, forKey: "shadowOffset.width")
        
        UIView.animateWithDuration(duration, delay: 0, options: Constants.TransitionOptions, animations: { () -> Void in
            fromView.frame = CGRect(origin: CGPoint(x: fromView.frame.width / -3, y: 0), size: fromView.frame.size)
            
            toView.frame = CGRect(origin: CGPoint.zero, size: toView.frame.size)
            
//            tabBar.frame = CGRect(origin: CGPoint(x: 0, y: fromView.frame.height), size: tabBar.frame.size)
            
            }, completion: { (_) -> Void in
                
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        })
        
    }
    
}
