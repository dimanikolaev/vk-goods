//
//  TovarTableViewController.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 3/3/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import UIKit

class TovarTableViewController: UITableViewController, UIPageViewControllerDataSource {
    
    let goods = (UIApplication.sharedApplication().delegate as? AppDelegate)?.goods
    
    lazy var товар = Товар(dic: Dictionary<String, AnyObject>())
    
    let imageGalleryPageViewController = UIPageViewController(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        tableView.estimatedRowHeight = 100
        
        imageGalleryPageViewController.dataSource = self
        addChildViewController(imageGalleryPageViewController)
        imageGalleryPageViewController.didMoveToParentViewController(self)
        imageGalleryPageViewController.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: Selector("tapOnImageGallery")))
        
        if let vc = viewControllerAtIndex(0) {
            imageGalleryPageViewController.setViewControllers([vc], direction: .Forward, animated: false, completion: nil)
        }
        
        guard товар != nil else { return }
        
        let image = товар!.likeIt ? UIImage(named: "LikeSelected") : UIImage(named: "Like")
        
        likeButton.setImage(image, forState: .Normal)
    }
    
    override func viewDidAppear(animated: Bool) {
        if let navigationBarFrame = navigationController?.navigationBar.frame {
            topIndent = navigationBarFrame.origin.y + navigationBarFrame.height
        }
        
        guard товар != nil else { return }
        
        goods?.addUserVisit(товар: товар!)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("Tovar Gallery Cell", forIndexPath: indexPath) as! TovarGalleryTableViewCell
            imageGalleryPageViewController.view.frame = cell.galleryView.bounds
            cell.galleryView.addSubview(imageGalleryPageViewController.view)
            tovarGalleryCell = cell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("Tovar Price Cell", forIndexPath: indexPath) as! TovarPriceTableViewCell
            cell.priceLabel.text = товар?.priceText
            return cell
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier("Tovar Add To Bag Button Cell", forIndexPath: indexPath) as! TovarAddToBagButtonTableViewCell
            return cell
        case 3:
            let cell = tableView.dequeueReusableCellWithIdentifier("Tovar Title Cell", forIndexPath: indexPath) as! TovarTitleTableViewCell
            cell.title = товар?.title
            return cell
        case 4:
            let cell = tableView.dequeueReusableCellWithIdentifier("Tovar Description Cell", forIndexPath: indexPath) as! TovarDescriptionTableViewCell
            cell.descr = товар?.descr
            return cell
        default:
            return UITableViewCell()
        }
    }

    
    // MARK: - Table view delegate
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
        case 0: return view.frame.width
        case 1: return 52
        case 2: return 72
        case 3, 4: return UITableViewAutomaticDimension
        default: return 0
        }
    }
    
    
    // MARK: UICollectionViewDelegate
    
    var topIndent: CGFloat = 0
    
    var tovarGalleryCell: TovarGalleryTableViewCell?
    
    let tovarGalleryCellHeight = UIScreen.mainScreen().bounds.width
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= -topIndent {
            tovarGalleryCell?.frame = CGRect(x: 0, y: scrollView.contentOffset.y + topIndent, width: tovarGalleryCell!.frame.width, height: tovarGalleryCellHeight - scrollView.contentOffset.y - topIndent)
        }
    }
    
    
    // MARK: - Navigation

    
    @IBOutlet weak var commentsBarButtonItem: UIBarButtonItem! {
        didSet {
            commentsBarButtonItem.tintColor = Constants.mainColor
        }
    }
    
    @IBAction func commentsBarButtonItemTap(sender: AnyObject) {
        performSegueWithIdentifier("Show Comments", sender: nil)
    }
    
    
    @IBOutlet weak var likeButton: UIButton! {
        didSet {
            likeButton.tintColor = Constants.mainColor
        }
    }
    
    
    @IBAction func likeButtonTap() {
        
        guard let like = товар?.likeIt else { return }
        
        товар?.likeIt = !like
        
        let image = !like ? UIImage(named: "LikeSelected") : UIImage(named: "Like")
        
        likeButton.setImage(image, forState: .Normal)
    }
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard segue.identifier == "Show Comments" else { return }
        guard let commentsVC = segue.destinationViewController as? CommentsViewController else { return }
        guard товар != nil else { return }
        
        commentsVC.товар = товар
    }
    
    
    override func prefersStatusBarHidden() -> Bool {
        return view.hidden
    }
    
    
    // MARK: - Page view controller data source
    
    func viewControllerAtIndex(index: Int) -> ImageGalleryContentViewController? {
        
        guard index >= 0 && index < товар?.imagesCount else { return nil }
        
        return ImageGalleryContentViewController(товар: товар!, index: index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        guard let index = (viewController as? ImageGalleryContentViewController)?.index else { return nil }
        
        return viewControllerAtIndex(index - 1)
    }
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        guard let index = (viewController as? ImageGalleryContentViewController)?.index else { return nil }
        
        return viewControllerAtIndex(index + 1)
    }
    
    func tapOnImageGallery() {
        if view.hidden {
            if let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? TovarGalleryTableViewCell {
                if navigationController != nil {
                    UIView.transitionWithView(imageGalleryPageViewController.navigationController!.view, duration: 0.25, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
                        self.imageGalleryPageViewController.view.frame = cell.galleryView.bounds
                        cell.galleryView.addSubview(self.imageGalleryPageViewController.view)
                        }, completion: nil)
                }
            }
            
            view.hidden = false
            navigationController?.navigationBar.hidden = false
            tabBarController?.tabBar.hidden = false
            setNeedsStatusBarAppearanceUpdate()
        } else {
            if navigationController != nil {
                UIView.transitionWithView(navigationController!.view, duration: 0.25, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
                    self.navigationController?.view.addSubview(self.imageGalleryPageViewController.view)
                    }, completion: nil)
            }
            
            imageGalleryPageViewController.view.frame = view.frame
            view.hidden = true
            navigationController?.navigationBar.hidden = true
            tabBarController?.tabBar.hidden = true
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    
    // MARK: - Notifications
    
    
    func updateGallery(notification: NSNotification) {
//        let count = notification.userInfo!["count"] as! Int
//        print("count: \(count)")
//        
//        guard (notification.object as? Товар) == товар else { return }
//        
//        guard count == 4 else { return }
        
//        if let vc = viewControllerAtIndex(0) {
//            imageGalleryPageViewController.setViewControllers([vc], direction: .Forward, animated: false, completion: nil)
//        }
    }

}
