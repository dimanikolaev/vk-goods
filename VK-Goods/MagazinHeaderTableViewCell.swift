//
//  MagazinHeaderTableViewCell.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/26/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit

class MagazinHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
