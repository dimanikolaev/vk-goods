//
//  PopDownAnimator.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/29/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//


class PopDownAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var transitionShouldBeCompleted = false
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return Constants.TransitionDuration
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        defer {
            if !transitionShouldBeCompleted {
                print("defer")
                transitionContext.completeTransition(false)
            }
        }
        
        guard   let containerView = transitionContext.containerView(),
            fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? PodborkaCollectionViewController,
            fromView = fromViewController.view,
            toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as? PodborkasTableViewControllerProtocol,
            toView = toViewController.tableView else { return }
        
        transitionShouldBeCompleted = true
        
        let maskRect = CGRect(origin: CGPoint(x: 0, y: 64), size: CGSize(width: toView.frame.width, height: toView.frame.height))
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(rect: maskRect).CGPath
        
        fromView.layer.mask = maskLayer
        
        toView.frame = CGRect(origin: CGPoint.zero, size: toView.frame.size)
        
        containerView.insertSubview(toView, belowSubview: fromView)
        
        toViewController.selectedCell?.hidden = true
        
        var imageView: UIImageView?
        
        var titleView: UIView?
        var titleLabel: UILabel?
        
        if let cellTitleView = toViewController.selectedCell?.titleView, cellTitleLabel = toViewController.selectedCell?.titleLabel {
            imageView = UIImageView(image: fromViewController.подборка?.coverImage ?? UIImage(named: "Marketplace"))
            imageView?.frame = CGRect(origin: CGPoint(x: 0, y: 64), size: toViewController.selectedCellFrame.size)
            imageView?.contentMode = .ScaleAspectFill
            imageView?.clipsToBounds = true
            containerView.addSubview(imageView!)
            
            let maskRect = CGRect(origin: CGPoint(x: 0, y: 64 + imageView!.frame.height), size: CGSize(width: toView.frame.width, height: toView.frame.height))
            let maskLayer = CAShapeLayer()
            maskLayer.path = UIBezierPath(rect: maskRect).CGPath
            
            fromView.layer.mask = maskLayer
            
            titleView = UIView(frame: cellTitleView.frame)
            titleView?.backgroundColor = cellTitleView.backgroundColor
            titleView?.alpha = 0
            
            titleLabel = UILabel(frame: cellTitleLabel.frame)
            titleLabel?.text = cellTitleLabel.text
            titleLabel?.font = cellTitleLabel.font
            titleLabel?.textColor = cellTitleLabel.textColor
            titleLabel?.backgroundColor = titleView!.backgroundColor
            
            titleView?.addSubview(titleLabel!)
            imageView?.addSubview(titleView!)
        } else {
            let maskRect = CGRect(origin: CGPoint(x: 0, y: 64), size: CGSize(width: toView.frame.width, height: toView.frame.height))
            let maskLayer = CAShapeLayer()
            maskLayer.path = UIBezierPath(rect: maskRect).CGPath
            
            fromView.layer.mask = maskLayer
        }
        
        UIView.animateWithDuration(transitionDuration(transitionContext), delay: 0, options: Constants.TransitionOptions, animations: { () -> Void in
            fromView.alpha = 0
            toView.alpha = 1
            titleView?.alpha = 1
            imageView?.frame = CGRect(origin: CGPoint(x: 0, y: toViewController.selectedCellFrame.origin.y + 64), size: imageView!.frame.size)
            fromView.frame = CGRect(origin: toViewController.selectedCellFrame.origin, size: fromView.frame.size)
            }, completion: { (_) -> Void in
                imageView?.removeFromSuperview()
                fromView.layer.mask = nil
                toViewController.selectedCell?.hidden = false
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        })
    }
    
}

