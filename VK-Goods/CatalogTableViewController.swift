//
//  CatalogTableViewController.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/14/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit
import CoreData

class CatalogTableViewController: UITableViewController {
    
    
    // MARK: - Properties
    
    
    let goods = (UIApplication.sharedApplication().delegate as? AppDelegate)?.goods
    
    var groupId: String = ""
    
    
    // MARK: Table view
    
    
    let podborkaCellHeight = Constants.PodborkaCellHeight
    
    
    // MARK: Data
    
    
    var подборки = Array<Подборка>() {
        didSet {
            print(подборки)
            tableView.reloadData()
            
            // Это нужно, чтобы ячейки таблицы не "выпадали сверху", а вместо этого, таблица появлялась уже полность загруженной
            if tableView.hidden {
                tableView.hidden = false
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.tableView.alpha = 1
                })
            }
        }
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updatePodborkaFromNotification:", name: "podborkaHasBeenUpdated", object: nil)
        
        tableView.hidden = true
        tableView.alpha = 0
//        
//        goods?.getPodborkasOfGroupWithId(groupId) { (подборки, error) -> Void in
//            guard error == nil else { return }
//            
//            self.подборки = подборки
//        }
        
        if let navigationBarFrame = navigationController?.navigationBar.frame {
            selectedCellFrameIndent = navigationBarFrame.origin.y + navigationBarFrame.height
        }
    }
    
    
    deinit {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    // MARK: - Table view delegate
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return podborkaCellHeight
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        selectedCell = tableView.cellForRowAtIndexPath(indexPath) as? PodborkaTableViewCellDefault
        selectedCell?.hidden = true
        
        performSegueWithIdentifier("Show Podborka", sender: indexPath)
    }
    
    
    // MARK: - Table view data source
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {

        return 1
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return подборки.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let подборка = подборки[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Podborka Default Cell", forIndexPath: indexPath) as! PodborkaTableViewCellDefault
        cell.coverImageView.image = подборка.coverImage
        cell.titleLabel.text = подборка.title
        
        return cell
    }
    
    
    // MARK: - Navigation
    
    
    var selectedCellFrameIndent: CGFloat = 0
    var selectedCellFrame = CGRect.zero
    var selectedCell: PodborkaTableViewCellDefault?
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard let   podborkaViewController = segue.destinationViewController as? PodborkaCollectionViewController,
                    index = (sender as? NSIndexPath)?.row where index < подборки.count else { return }
        
        let rect = tableView.rectForRowAtIndexPath(sender as! NSIndexPath)
        
        selectedCellFrame = tableView.convertRect(CGRect(origin: CGPoint(x: 0, y: rect.origin.y - tableView.contentOffset.y - selectedCellFrameIndent), size: rect.size), fromView: view)
        
        podborkaViewController.подборка = подборки[index]
    }
    
    
    // MARK: - Notifications
    
    
    func updatePodborkaFromNotification(notification: NSNotification) {
        
        guard let   подборка = notification.object as? Подборка,
                    index = подборки.indexOf({ $0 == подборка }),
                    cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as? PodborkaTableViewCellDefault else { return }
        
        UIView.transitionWithView(cell.coverImageView, duration: 0.25, options: .TransitionCrossDissolve, animations: { () -> Void in
            cell.coverImageView.image = подборка.coverImage
            }, completion: nil)
    }
}
