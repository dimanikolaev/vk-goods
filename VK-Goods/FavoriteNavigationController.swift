//
//  FavoriteNavigationController.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/21/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit

class FavoriteNavigationController: UINavigationController, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    let goods = (UIApplication.sharedApplication().delegate as? AppDelegate)?.goods
    
    var interactionController: UIPercentDrivenInteractiveTransition?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        
        let sreenEdgeGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: Selector("panned:"))
        sreenEdgeGestureRecognizer.edges = .Left
        sreenEdgeGestureRecognizer.delegate = self
        view.addGestureRecognizer(sreenEdgeGestureRecognizer)
    }
    
    
    override func viewWillAppear(animated: Bool) {
        
        if viewControllers.count == 1 {
            guard let favoriteCollectionViewController = viewControllers.first as? FavoriteCollectionViewController else { return }
            
            if favoriteCollectionViewController.shouldLoad {
                favoriteCollectionViewController.товары.removeAll()
                favoriteCollectionViewController.collectionView?.reloadData()
                
                favoriteCollectionViewController.shouldLoad = false
                
                if favoriteCollectionViewController.activityIndicatorView == nil {
                    
                    favoriteCollectionViewController.activityIndicatorView = UIActivityIndicatorView(frame: view.frame)
                    favoriteCollectionViewController.activityIndicatorView?.activityIndicatorViewStyle = .Gray
                    favoriteCollectionViewController.view.addSubview(favoriteCollectionViewController.activityIndicatorView!)
                    favoriteCollectionViewController.activityIndicatorView?.startAnimating()
                }
                
                goods?.getFavoriteTovarsUseOffset(0, withMaxCount: Constants.QuantityPerOneRequest, inBackgroundWithBlock: { (товары, error) -> Void in
                    if favoriteCollectionViewController.activityIndicatorView != nil {
                        
                        favoriteCollectionViewController.activityIndicatorView?.removeFromSuperview()
                        favoriteCollectionViewController.activityIndicatorView = nil
                    }
                    favoriteCollectionViewController.shouldLoad = true
                })
            }
            
        }
    }
    
    
    // MARK: - Screen edge pan gesture recognizer
    
    func panned(recognizer: UIScreenEdgePanGestureRecognizer) {
        switch recognizer.state {
        case .Began:
            if viewControllers.count > 1 {
                interactionController = UIPercentDrivenInteractiveTransition()
                popViewControllerAnimated(true)
            }
        case .Changed:
            let translation = recognizer.translationInView(view)
            let progress = translation.x / view.bounds.width
            interactionController?.updateInteractiveTransition(progress)
        case .Ended:
            recognizer.velocityInView(view).x > 0 ? interactionController?.finishInteractiveTransition() : interactionController?.cancelInteractiveTransition()
            interactionController = nil
        default:
            interactionController?.cancelInteractiveTransition()
            interactionController = nil
        }
    }
    
    // MARK: - Screen edge pan gesture recognizer delegate
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    
    // MARK: - Navigation controller delegate
    
    
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        switch operation {
            
        case .Push:
            if fromVC is FavoriteCollectionViewController {
                return PushInAnimator()
            }
            
            if toVC is CommentsViewController {
                return PushToChatAnimator()
            }
            
            return PushAnimator()
            
        case .Pop:
            if fromVC is CommentsViewController {
                return PopFromChatAnimator()
            }
            
            return PopAnimator()
            
        default:
            return nil
        }
    }
    
    
    func navigationController(navigationController: UINavigationController, interactionControllerForAnimationController animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        return interactionController
    }
    
}

