//
//  TovarDescriptionTableViewCell.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 3/12/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import UIKit

class TovarDescriptionTableViewCell: UITableViewCell {
    
    var descr: String? = "" {
        didSet {
            let attributeString = NSMutableAttributedString(string: descr ?? "")
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 3
            attributeString.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSMakeRange(0, attributeString.string.characters.count))
            descriptionLabel.attributedText = attributeString
        }
    }

    @IBOutlet private weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
