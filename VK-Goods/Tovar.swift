//
//  Tovar.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/29/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//


typealias Tovar = Товар

class Товар: Equatable, CustomStringConvertible {
    
    let id: Int
    let ownerId: Int
    var title: String
    var descr: String
    var thumbPhoto: UIImage?
    var priceText: String
    var precisePrice: Double
    var images = Array<UIImage>()
    var imagesCount: Int = 1
    var likeIt: Bool = false {
        didSet {
            let goods = (UIApplication.sharedApplication().delegate as? AppDelegate)?.goods
            likeIt ? goods?.addLikeToTovar(self) : goods?.takeLikeBack(self)
            NSNotificationCenter.defaultCenter().postNotificationName("tovarHasBeenUpdated", object: self)
        }
    }
    
    init?(dic: Dictionary<String, AnyObject>) {
        guard
            let id = dic["id"] as? Int,
            ownerId = dic["owner_id"] as? Int,
            title = dic["title"] as? String,
            _ = dic["thumb_photo"] as? String,
            price = dic["price"]?["amount"] as? String,
            likeIt = dic["likes"]?["user_likes"] as? Bool
            else {
                self.id = 0
                self.ownerId = 0
                self.title = ""
                self.descr = ""
                self.priceText = ""
                self.precisePrice = 0
                return nil
        }
        
        self.id = id
        self.ownerId = ownerId
        self.title = title
        self.descr = dic["description"] as? String ?? ""
        self.likeIt = likeIt
        
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        formatter.maximumFractionDigits = 0
        formatter.locale = NSLocale(localeIdentifier: "ru_RU")
        
        self.priceText = formatter.stringFromNumber(Int(price)! / 100)!
        
        self.precisePrice = Double(price)! / 100
        
//        guard
//            let thumbURL = NSURL(string: thumbPhotoURLString),
//            thumbPhotoData = NSData(contentsOfURL: thumbURL)
//            else {
//                return nil
//        }
        
//        if let thumb = UIImage(data: thumbPhotoData) {
//            thumbPhoto = thumb
//            images = [thumb]
//        }
        
        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) { () -> Void in
        guard let photos = dic["photos"] as? [Dictionary<String, AnyObject>] where !photos.isEmpty else { return nil }
        
        imagesCount = photos.count
        
        var thumbURLString = ""
        if let photoURLString =  photos[0]["photo_604"] as? String {
            thumbURLString = photoURLString
        } else if let photoURLString =  photos[0]["photo_130"] as? String {
            thumbURLString = photoURLString
        }
        
        if let thumbURL = NSURL(string: thumbURLString) {
            
            guard let   data = NSData(contentsOfURL: thumbURL),
                        photo = UIImage(data: data) else { return nil }
            
            self.thumbPhoto = photo
            self.images = [photo]
            
            return
            
            let request = NSMutableURLRequest(URL: thumbURL)
            request.HTTPShouldHandleCookies = false
            
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
                guard error == nil else { print("ERROR: \(error!.debugDescription)"); return }
                guard response != nil && data != nil else { return }
                if let photo = UIImage(data: data!) {
                    dispatch_async(dispatch_get_main_queue()) { () -> Void in
                        self.thumbPhoto = photo
                        self.images = [photo]
                        NSNotificationCenter.defaultCenter().postNotificationName("tovarHasBeenUpdated", object: self)
                    }
                }
            }
            
            task.resume()
        }
//
//            if  let thumbURL = NSURL(string: thumbURLString),
//                data = NSData(contentsOfURL: thumbURL),
//                thumbPhoto = UIImage(data: data) {
//                    self.thumbPhoto = thumbPhoto
//                    self.images.append(thumbPhoto)
//                    NSNotificationCenter.defaultCenter().postNotificationName("tovarHasBeenUpdated", object: self, userInfo: nil)
//            }
            
//            var photosURLs = Array<NSURL>()
//            
//            for photo in photos {
//                var str = ""
//                if let photoURLString = photo["photo_1280"] as? String {
//                    str = photoURLString
//                } else if let photoURLString = photo["photo_807"] as? String {
//                    str = photoURLString
//                } else if let photoURLString = photo["photo_604"] as? String {
//                    str = photoURLString
//                }
//                if let url = NSURL(string: str) {
//                    photosURLs.append(url)
//                }
//            }
//
//            // Здесь мы немного хитрим. Сначала добавляем в массив фотографий товара фото `thumbPhoto`. Но как только у нас появляется первая фотография в хорошем качестве, присваиваем `images` новый массив с этой фотографией.
//            
//            var firstImage = true
//            
//            for url in photosURLs {
//                
//                let request = NSMutableURLRequest(URL: url)
//                request.HTTPMethod = "GET"
//                request.HTTPShouldHandleCookies = false
//
//                let session = NSURLSession.sharedSession()
//
//                let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
//                    guard error == nil else { print("ERROR: \(error!.debugDescription)"); return }
//                    guard response != nil && data != nil else { return }
//                    if let photo = UIImage(data: data!) {
//                        if firstImage {
//                            firstImage = false
//                            self.images = [photo]
//                        } else {
//                            self.images.append(photo)
//                        }
//                    }
//                }
//                
//                task.resume()
//            }
        
    }
    
    
    // MARK: CustomStringConvertible protocol
    
    var description: String {
        
        return "\nТовар\n-----\nID: \(id)\nOWNER_ID: \(ownerId)\nPRICE: \(priceText)\nDESCR:\n    \(descr)"
    }
    
}

// MARK: - Equatable protocol

func ==(lhs: Tovar, rhs: Tovar) -> Bool {
    return lhs.id == rhs.id
}