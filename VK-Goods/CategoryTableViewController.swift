//
//  CategoryTableViewController.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/25/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit
import CoreData

class CategoryTableViewController: UITableViewController {
    
    let goods = (UIApplication.sharedApplication().delegate as? AppDelegate)?.goods
    
    lazy var categoryId: Int = 0
    lazy var categoryName: String = ""
    
    var магазины = Array<Магазин>()
    
    var activityIndicatorView: UIActivityIndicatorView?
    
    var hasNoMarketsLabel: UILabel?
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        activityIndicatorView = UIActivityIndicatorView(frame: tableView.frame)
        activityIndicatorView?.activityIndicatorViewStyle = .Gray
        tableView.addSubview(activityIndicatorView!)
        navigationController?.view.insertSubview(activityIndicatorView!, aboveSubview: tableView)
        activityIndicatorView?.startAnimating()

        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateMagazFromNotification:", name: "magazHasBeenUpdated", object: nil)
        
        
        goods?.getMagazinsForCategoryWithId(categoryId, inBackgroundWithBlock: { (магазины, error) -> Void in
            
            self.магазины = магазины.reverse()
            
            self.activityIndicatorView?.removeFromSuperview()
            self.activityIndicatorView = nil
            
            if магазины.isEmpty {
                self.hasNoMarketsLabel = UILabel(frame: self.tableView.frame)
                self.hasNoMarketsLabel!.textAlignment = .Center
                self.hasNoMarketsLabel!.font = UIFont.systemFontOfSize(15)
                self.hasNoMarketsLabel!.textColor = .lightGrayColor()
                self.hasNoMarketsLabel!.text = "No markets"
                self.navigationController?.view.insertSubview(self.hasNoMarketsLabel!, aboveSubview: self.tableView)
                
                return
            }
            
            self.tableView.reloadData()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        
        activityIndicatorView?.removeFromSuperview()
        hasNoMarketsLabel?.removeFromSuperview()
    }
    
    deinit {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
//        tableView.hidden = магазины.isEmpty
        
        return магазины.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let магазин = магазины[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Podborka Default Cell", forIndexPath: indexPath) as! PodborkaTableViewCellDefault
        
        cell.titleLabel.text = магазин.title
        cell.coverImageView.image = магазин.photo
        
        return cell
    }
    
    
    // MARK: - Table view delegate
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return Constants.PodborkaCellHeight
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        performSegueWithIdentifier("Show Magazin", sender: indexPath)
    }
    
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 1, height: 1)))
    }
 
    
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard let   magazinViewController = segue.destinationViewController as? MagazinTableViewController,
                    index = (sender as? NSIndexPath)?.row where index < магазины.count else { return }
        
        magazinViewController.магазин = магазины[index]
    }
    
    
    
    // MARK: - Notifications
    
    
    func updateMagazFromNotification(notification: NSNotification) {
        
        guard let   магазин = notification.object as? Магазин,
                    index = магазины.indexOf({ $0 == магазин }),
                    cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as? PodborkaTableViewCellDefault
                    else { return }
        
        UIView.transitionWithView(cell.coverImageView, duration: 0.25, options: .TransitionCrossDissolve, animations: { () -> Void in
            cell.coverImageView.image = магазин.photo
            }, completion: nil)
    }

}
