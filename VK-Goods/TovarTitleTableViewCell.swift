//
//  TovarTitleTableViewCell.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 3/12/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import UIKit

class TovarTitleTableViewCell: UITableViewCell {
    
    var title: String? = "" {
        didSet {
            let attributeString = NSMutableAttributedString(string: title ?? "")
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 3
            attributeString.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSMakeRange(0, attributeString.string.characters.count))
            titleLabel.attributedText = attributeString
        }
    }

    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
