//
//  LogInViewController.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/16/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import UIKit
import VK_ios_sdk

class LogInViewController: UIViewController {
    
    let goods = (UIApplication.sharedApplication().delegate as? AppDelegate)?.goods
    
    // Добавлям наш аниматор смены вью контроллера
    
    let animatedTransitioning = SignInAnimatedTransitioning()
    
    // Добавляем медод и проперти для кнопки
    
    
    @IBOutlet weak var logInThroughVKButton: UIButton! {
        
        didSet {
            logInThroughVKButton.setTitle("Log in with VK".localized.uppercaseString, forState: .Normal)
            logInThroughVKButton.backgroundColor = UIColor(hue: 0.58, saturation: 0.68, brightness: 0.99, alpha: 1)
            logInThroughVKButton.layer.cornerRadius = 24
        }
    }
    
    
    @IBAction func logInThroughVKButtonTap() {
        if NSUserDefaults.standardUserDefaults().boolForKey("userIsAlreadyLoggedIn") {
            logInThroughVKButton.hidden = true
            performSegueWithIdentifier("Present Tab Bar Controller", sender: nil)
        } else {
            goods?.authorizeThroughVK()
        }
    }
    
    
    lazy var logoView = UIView()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Добавляем градиент, потому что градиент всё делает лучше

        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.frame

        let topColor = UIColor(hue: 0.58, saturation: 0.6, brightness: 0.84, alpha: 1).CGColor
        let bottomColor = UIColor(hue: 0.58, saturation: 0.68, brightness: 0.68, alpha: 1).CGColor
        
        gradientLayer.colors = [topColor, bottomColor]
        gradientLayer.locations = [0, 1]
        
        view.layer.insertSublayer(gradientLayer, atIndex: 0)
        
        // Добавляем параллакс эффект, потому что параллакс эффект всё делает лучще
        
        let value = 50
        
        let bgView = UIView(frame: CGRect(x: -0.333333 * view.bounds.width, y: -0.333333 * view.bounds.height, width: view.bounds.width * 1.5, height: view.bounds.height * 1.5))
        bgView.backgroundColor = UIColor(patternImage: UIImage(named: "testbg-1")!)
        
        let horizontalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x", type: .TiltAlongHorizontalAxis)
        horizontalMotionEffect.minimumRelativeValue = -value
        horizontalMotionEffect.maximumRelativeValue = value
        
        let verticalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y", type: .TiltAlongVerticalAxis)
        verticalMotionEffect.minimumRelativeValue = -value
        verticalMotionEffect.maximumRelativeValue = value
        
        bgView.addMotionEffect(horizontalMotionEffect)
        bgView.addMotionEffect(verticalMotionEffect)
        
        view.insertSubview(bgView, belowSubview: logInThroughVKButton)
        view.clipsToBounds = true
        
        // Добавляем логотип ВК
        
        logoView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 144))
        logoView.backgroundColor = UIColor(hue: 0.58, saturation: 0.68, brightness: 0.68, alpha: 1)
//        logoView.backgroundColor = UIColor(hue: 0.58, saturation: 0.77, brightness: 0.99, alpha: 1)
        
        logoView.layer.mask = getVKMaskLayerForSize(logoView.bounds.size)
        view.insertSubview(logoView, belowSubview: logInThroughVKButton)
    }
    
    
    func getVKMaskLayerForSize(size: CGSize) -> CAShapeLayer {
        // Благодаря этой 👇 замечательной проге мы можем сделать `UIBezierPath` из .eps логотипа Вконтакте        
        //// PaintCode Trial Version
        //// www.paintcodeapp.com
        
        let vkShapePath = UIBezierPath()
        vkShapePath.moveToPoint(CGPointMake(47.02, 54.93))
        vkShapePath.addLineToPoint(CGPointMake(52.75, 54.93))
        vkShapePath.addCurveToPoint(CGPointMake(55.37, 53.78), controlPoint1: CGPointMake(52.75, 54.93), controlPoint2: CGPointMake(54.48, 54.74))
        vkShapePath.addCurveToPoint(CGPointMake(56.15, 51.25), controlPoint1: CGPointMake(56.18, 52.9), controlPoint2: CGPointMake(56.15, 51.25))
        vkShapePath.addCurveToPoint(CGPointMake(59.61, 42.32), controlPoint1: CGPointMake(56.15, 51.25), controlPoint2: CGPointMake(56.04, 43.45))
        vkShapePath.addCurveToPoint(CGPointMake(72.44, 53.07), controlPoint1: CGPointMake(63.13, 41.2), controlPoint2: CGPointMake(67.65, 49.78))
        vkShapePath.addCurveToPoint(CGPointMake(78.81, 54.93), controlPoint1: CGPointMake(76.06, 55.57), controlPoint2: CGPointMake(78.81, 54.93))
        vkShapePath.addLineToPoint(CGPointMake(91.62, 54.94))
        vkShapePath.addCurveToPoint(CGPointMake(95.15, 49.14), controlPoint1: CGPointMake(91.62, 54.94), controlPoint2: CGPointMake(98.32, 54.43))
        vkShapePath.addCurveToPoint(CGPointMake(85.62, 38.09), controlPoint1: CGPointMake(94.89, 48.71), controlPoint2: CGPointMake(93.29, 45.24))
        vkShapePath.addCurveToPoint(CGPointMake(88.34, 18.89), controlPoint1: CGPointMake(77.59, 30.62), controlPoint2: CGPointMake(78.67, 31.83))
        vkShapePath.addCurveToPoint(CGPointMake(95.85, 4.15), controlPoint1: CGPointMake(94.23, 11.02), controlPoint2: CGPointMake(96.59, 6.21))
        vkShapePath.addCurveToPoint(CGPointMake(90.85, 2.53), controlPoint1: CGPointMake(95.15, 2.19), controlPoint2: CGPointMake(90.85, 2.53))
        vkShapePath.addLineToPoint(CGPointMake(76.4, 2.52))
        vkShapePath.addCurveToPoint(CGPointMake(74.53, 2.89), controlPoint1: CGPointMake(76.4, 2.52), controlPoint2: CGPointMake(75.33, 2.42))
        vkShapePath.addCurveToPoint(CGPointMake(73.26, 4.56), controlPoint1: CGPointMake(73.76, 3.36), controlPoint2: CGPointMake(73.26, 4.56))
        vkShapePath.addCurveToPoint(CGPointMake(67.94, 15.96), controlPoint1: CGPointMake(73.26, 4.56), controlPoint2: CGPointMake(70.98, 10.77))
        vkShapePath.addCurveToPoint(CGPointMake(57.9, 26.79), controlPoint1: CGPointMake(61.51, 26.89), controlPoint2: CGPointMake(58.95, 27.47))
        vkShapePath.addCurveToPoint(CGPointMake(56.07, 17.04), controlPoint1: CGPointMake(55.45, 25.21), controlPoint2: CGPointMake(56.07, 20.43))
        vkShapePath.addCurveToPoint(CGPointMake(52.94, 0.88), controlPoint1: CGPointMake(56.07, 6.44), controlPoint2: CGPointMake(57.67, 2.02))
        vkShapePath.addCurveToPoint(CGPointMake(46.21, 0.21), controlPoint1: CGPointMake(51.38, 0.5), controlPoint2: CGPointMake(50.22, 0.25))
        vkShapePath.addCurveToPoint(CGPointMake(34.25, 1.43), controlPoint1: CGPointMake(41.07, 0.15), controlPoint2: CGPointMake(36.71, 0.22))
        vkShapePath.addCurveToPoint(CGPointMake(32.11, 4.14), controlPoint1: CGPointMake(32.61, 2.24), controlPoint2: CGPointMake(31.34, 4.03))
        vkShapePath.addCurveToPoint(CGPointMake(36.37, 6.28), controlPoint1: CGPointMake(33.07, 4.27), controlPoint2: CGPointMake(35.22, 4.72))
        vkShapePath.addCurveToPoint(CGPointMake(37.79, 12.83), controlPoint1: CGPointMake(37.85, 8.3), controlPoint2: CGPointMake(37.79, 12.83))
        vkShapePath.addCurveToPoint(CGPointMake(35.81, 26.86), controlPoint1: CGPointMake(37.79, 12.83), controlPoint2: CGPointMake(38.64, 25.31))
        vkShapePath.addCurveToPoint(CGPointMake(25.48, 15.83), controlPoint1: CGPointMake(33.87, 27.92), controlPoint2: CGPointMake(31.2, 25.75))
        vkShapePath.addCurveToPoint(CGPointMake(20.33, 5.02), controlPoint1: CGPointMake(22.54, 10.74), controlPoint2: CGPointMake(20.33, 5.02))
        vkShapePath.addCurveToPoint(CGPointMake(19.14, 3.41), controlPoint1: CGPointMake(20.33, 5.02), controlPoint2: CGPointMake(19.9, 3.97))
        vkShapePath.addCurveToPoint(CGPointMake(16.92, 2.51), controlPoint1: CGPointMake(18.22, 2.73), controlPoint2: CGPointMake(16.92, 2.51))
        vkShapePath.addLineToPoint(CGPointMake(3.22, 2.52))
        vkShapePath.addCurveToPoint(CGPointMake(0.41, 3.47), controlPoint1: CGPointMake(3.22, 2.52), controlPoint2: CGPointMake(1.16, 2.57))
        vkShapePath.addCurveToPoint(CGPointMake(0.36, 5.92), controlPoint1: CGPointMake(-0.26, 4.27), controlPoint2: CGPointMake(0.36, 5.92))
        vkShapePath.addCurveToPoint(CGPointMake(23.23, 43.97), controlPoint1: CGPointMake(0.36, 5.92), controlPoint2: CGPointMake(11.08, 31.29))
        vkShapePath.addCurveToPoint(CGPointMake(47.02, 54.93), controlPoint1: CGPointMake(34.37, 55.61), controlPoint2: CGPointMake(47.02, 54.93))
        vkShapePath.closePath()
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = vkShapePath.CGPath
        maskLayer.frame.origin = CGPoint(x: size.width / 2 - 48, y: size.height / 2 - 27.5)
        
        return maskLayer
    }
    
    
    override func viewDidAppear(animated: Bool) {
        
        if NSUserDefaults.standardUserDefaults().boolForKey("userIsAlreadyLoggedIn") {
            logInThroughVKButton.hidden = true
            performSegueWithIdentifier("Present Tab Bar Controller", sender: nil)
        }
    }
    
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prefersStatusBarHidden() -> Bool {
        
        return true
    }
    
    
    // MARK: - Navigation

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard let tabBarController = segue.destinationViewController as? UITabBarController else { return }
        tabBarController.transitioningDelegate = self
    }
    

}


// Добавляем собственную анимацию смены вью контроллеров с помощью экстеншена для транзишенинг делегата нашего вью контроллера

extension LogInViewController: UIViewControllerTransitioningDelegate {

    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    
        return animatedTransitioning
    }
    
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return nil
    }
    
}
