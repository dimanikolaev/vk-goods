//
//  SignInAnimatedTransitioning.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/17/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import UIKit

class SignInAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
    
    var transitionShouldBeCompleted = false
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.7
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        // Мы используем `UITransitionContextToVIEWCONTROLLERKey` вместо `UITransitionContextToVIEWKey` и `UITransitionContextFromVIEWKey`, так-как они доступны начиная с 8.0, а наш минимальный деплоймент таргет 7.0
        
        guard let containerView = transitionContext.containerView(),
              let logoView = (transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? LogInViewController)?.logoView,
              let toView = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)?.view
              else { return }
        
        transitionShouldBeCompleted = true
        
        defer {
            if transitionShouldBeCompleted {
                let scale = containerView.bounds.width / 16
                //
                let transform = CGAffineTransformTranslate(CGAffineTransformMakeScale(scale, scale), 0, 0)
                
                UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
                    logoView.transform = transform
                    }, completion: { (_) -> Void in
                        containerView.addSubview(toView)
                        transitionContext.completeTransition(true)
                })
            } else {
                transitionContext.completeTransition(false)
            }
        }
        
    }

}
