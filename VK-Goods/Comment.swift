//
//  Comment.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/27/16.
//  Copyright © 2016 Beresta. All rights reserved.
//


class Comment {
    
    let id: Int
    let authorId: Int
    var authorAva: UIImage?
    let text: String?
    
    init?(dic: Dictionary<String, AnyObject>) {
        guard let   id = dic["id"] as? Int,
                    authorId = dic["from_id"] as? Int
                else {
                    self.id = 0
                    self.authorId = 0
                    self.text = nil
                    return nil
            }
        
        self.id = id
        self.authorId = authorId
        
        if var text = dic["text"] as? String {
            if text.characters.first == "[" {
                if let firstIndex = text.characters.indexOf("|"), secondIndex = text.characters.indexOf("]") {
                    text.removeAtIndex(secondIndex)
                    text = text.substringFromIndex(firstIndex.advancedBy(1))
                }
            }
            self.text = text
        } else {
            self.text = nil
        }
    }
}
