//
//  PopularTableViewController.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/18/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit

class PopularTableViewController: UITableViewController, PodborkasTableViewControllerProtocol {
    
    let goods = (UIApplication.sharedApplication().delegate as? AppDelegate)?.goods

    var specials = Array<(title: String, podborka: Podborka)>()
    
    var activityIndicatorView: UIActivityIndicatorView? {
        willSet {
            if newValue == nil {
                
                activityIndicatorView?.removeFromSuperview()
            } else {
                
                newValue?.activityIndicatorViewStyle = .Gray
                newValue?.frame = tableView.frame
                newValue?.startAnimating()
                
                navigationController?.view.addSubview(newValue!)
            }
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        getSpecials()
        
        activityIndicatorView = UIActivityIndicatorView()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updatePodborkaFromNotification:", name: "podborkaHasBeenUpdated", object: nil)
        
        if let navigationBarFrame = navigationController?.navigationBar.frame {
            selectedCellFrameIndent = navigationBarFrame.origin.y + navigationBarFrame.height
        }
    }
    
    
    override func viewDidAppear(animated: Bool) {
        
        getSpecials(specialsDate: NSUserDefaults.standardUserDefaults().stringForKey("specialsDate") ?? "")
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getSpecials(specialsDate specialsDate: String = "") {
        goods?.getSpecialsInBackgroundWithBlock(specialsDate: specialsDate) { (specials, error) -> Void in
            guard !specials.isEmpty else {
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicatorView = nil
                }
                return
            }
            
            for special in specials {
                self.goods?.getPodborkaInBackgroundWithId(special.podborkaId, fromGroupWithId: special.groupId) { (podborka, error) -> Void in
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        self.activityIndicatorView = nil
                        
                        guard podborka != nil else { return }
                        
                        self.specials.append((title: special.title, podborka: podborka!))
                        
                        self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: self.specials.count - 1, inSection: 0)], withRowAnimation: .Fade)
                    }
                }
            }
        }
    }
    
    deinit {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        tableView.hidden = specials.isEmpty
        
        return specials.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let special = specials[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Podborka Default Cell", forIndexPath: indexPath) as! PodborkaTableViewCellDefault
        
        cell.titleLabel.text = special.title
        cell.coverImageView.image = special.podborka.coverImage

        return cell
    }
    
    
    // MARK: - Table view delegate
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return Constants.PodborkaCellHeight
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        selectedCell = tableView.cellForRowAtIndexPath(indexPath) as? PodborkaTableViewCellDefault
        selectedCell?.hidden = true
        
        performSegueWithIdentifier("Show Podborka", sender: indexPath)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Podborkas collection view controller protocol
    
    
    var selectedCellFrameIndent: CGFloat = 0
    var selectedCellFrame = CGRect.zero
    var selectedCell: PodborkaTableViewCellDefault?
    
    
    // MARK: - Navigation
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard let   podborkaViewController = segue.destinationViewController as? PodborkaCollectionViewController,
                    index = (sender as? NSIndexPath)?.row where index < specials.count else { return }
        
        let rect = tableView.rectForRowAtIndexPath(sender as! NSIndexPath)
        
        selectedCellFrame = tableView.convertRect(CGRect(origin: CGPoint(x: 0, y: rect.origin.y - tableView.contentOffset.y - selectedCellFrameIndent), size: rect.size), fromView: view)
        
        let podborka = specials[index].podborka
        
        podborkaViewController.подборка = podborka
        podborkaViewController.ownerId = podborka.ownerId
    }

    
    
    // MARK: - Notifications
    
    
    func updatePodborkaFromNotification(notification: NSNotification) {
        
        guard let   podborka = notification.object as? Подборка,
                    index = specials.indexOf({ $0.podborka == podborka }),
                    cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as? PodborkaTableViewCellDefault
        else { return }
        
        UIView.transitionWithView(cell.coverImageView, duration: 0.25, options: .TransitionCrossDissolve, animations: { () -> Void in
            cell.coverImageView.image = podborka.coverImage
            }, completion: nil)
    }

}
