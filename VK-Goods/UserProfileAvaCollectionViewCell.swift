//
//  UserProfileAvaCollectionViewCell.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/24/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit

class UserProfileAvaCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var avaImageView: UserProfileImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var logoutButton: UIButton! {
        didSet {
            logoutButton.layer.cornerRadius = 18
        }
    }
    
    @IBAction func logoutButtonTap() {
        
        (UIApplication.sharedApplication().delegate as? AppDelegate)?.goods.logout()
    }
    
    @IBOutlet weak var recentTovaryLabel: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
}

class UserProfileImageView: UIImageView {
    override var bounds: CGRect {
        willSet {
            let gradientLayer = CAGradientLayer()
            gradientLayer.frame = newValue
            
            //            let statusBarBackgroundColor = UIColor(white: 0.0, alpha: 0.1).CGColor as CGColorRef
            let statusBarBackgroundColor = UIColor(white: 0.0, alpha: 0.14).CGColor as CGColorRef
            let transparentColor = UIColor(white: 0.0, alpha: 0).CGColor as CGColorRef
            let bottomColor = UIColor(white: 0.0, alpha: 0.14).CGColor as CGColorRef
            
            gradientLayer.colors = [statusBarBackgroundColor, transparentColor, transparentColor, bottomColor]
            gradientLayer.locations = [0.025, 0.125, 0.666666, 0.85]
            
            layer.sublayers?[0].removeFromSuperlayer()
            layer.insertSublayer(gradientLayer, atIndex: 0)
        }
    }
}
