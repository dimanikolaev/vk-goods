//
//  PodborkaHeaderCollectionViewCell.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/28/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import UIKit

class PodborkaHeaderCollectionViewCell: UICollectionViewCell {
     
    @IBOutlet weak var headerImageView: UIImageView!
    
}
