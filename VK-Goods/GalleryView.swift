//
//  GalleryView.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 04/03/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import UIKit

class GalleryView: UIView {
    
    override var bounds: CGRect {
        didSet {
            subviews.forEach { $0.frame = bounds }
        }
    }
    
}
