//
//  PodborkaCollectionViewController.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/28/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class PodborkaCollectionViewController: UICollectionViewController, TovarsCollectionViewControllerProtocol, VKGoodsTovarnyDelegate {
    
    let goods = (UIApplication.sharedApplication().delegate as? AppDelegate)?.goods
    
    var товары = Array<Товар>()
    
    lazy var подборка = Подборка(dic: Dictionary<String, AnyObject>())
    
    var ownerId: Int = 0 
    
    var activityIndicatorView: UIActivityIndicatorView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateTovarFromNotification:", name: "tovarHasBeenUpdated", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateTovarLikeInCellFromNotification:", name: "likeButtonInCellHasBeenTapped", object: nil)
        
        collectionView?.alwaysBounceVertical = true
        
        let width = view.frame.width / 2.2
        tovarCellSize = CGSize(width: width, height: width + 112)
        
        headerCellSize = CGSize(width: view.frame.width, height: Constants.PodborkaCellHeight)
        
        let inset = (view.frame.width - width * 2) / 3.333333
        tovarsSectionEdgeInsets = UIEdgeInsets(top: 10, left: inset, bottom: 0, right: inset)
        
        activityIndicatorView = UIActivityIndicatorView(frame: view.frame)
        activityIndicatorView?.activityIndicatorViewStyle = .Gray
        view.addSubview(activityIndicatorView!)
        activityIndicatorView?.startAnimating()
        
        
        goods?.tovaryDelegate = self
        
        viewController = self
        
        print(ownerId)
        
        goods?.getTovaryFromGroupWithId(ownerId, belongsToPodborkaWithId: подборка?.id, usingOffset: 0, withMaxCount: quantityPerOneRequest, forViewController: viewController) { (товары, error) -> Void in }
    }
    
    override func viewDidAppear(animated: Bool) {
        if let navigationBarFrame = navigationController?.navigationBar.frame {
            selectedCellThumbPhotoTopIndent = navigationBarFrame.origin.y + navigationBarFrame.height
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("deinite")
        print(viewController)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        switch section {
        case 0:
            return 1
        case 1:
            return товары.count
        default:
            return 0
        }
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Podborka Header Cell", forIndexPath: indexPath) as! PodborkaHeaderCollectionViewCell
            cell.headerImageView.image = подборка?.coverImage ?? UIImage(named: "Marketplace")
            
            headerCell = cell
            
            return cell
        case 1:
            let товар = товары[indexPath.row]
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Tovar Cell", forIndexPath: indexPath) as! TovarCollectionViewCell
            cell.thumbImageView.image = товар.thumbPhoto
            cell.titleLabel.text = товар.title
            cell.priceLabel.text = товар.priceText
            cell.likeIt = товар.likeIt
            return cell
        default:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath)
            return cell
        }
    }

    // MARK: UICollectionViewDelegate
    
    var headerCell: UICollectionViewCell?
    
    let headerCellHeight = Constants.PodborkaCellHeight
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y <= -selectedCellThumbPhotoTopIndent {
            headerCell?.frame = CGRect(x: 0, y: scrollView.contentOffset.y + selectedCellThumbPhotoTopIndent, width: headerCell!.frame.width, height: headerCellHeight - scrollView.contentOffset.y - selectedCellThumbPhotoTopIndent)
        }
    }
    
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        guard indexPath.section == 1 && indexPath.row < товары.count else { return }
        
        selectedCell = collectionView.cellForItemAtIndexPath(indexPath) as? TovarCollectionViewCell
        
        guard let cellRect = collectionView.layoutAttributesForItemAtIndexPath(indexPath)?.frame else { return }
        
        guard selectedCell != nil else { return }
        
        selectedCellThumbPhotoFrame = CGRect(origin: CGPoint(x: cellRect.origin.x + selectedCell!.thumbImageView.frame.origin.x, y: cellRect.origin.y + selectedCell!.thumbImageView.frame.origin.y  - collectionView.contentOffset.y), size: selectedCell!.thumbImageView.frame.size)
        
        performSegueWithIdentifier("Show Tovar", sender: товары[indexPath.row])
    }
    
    
    
    // MARK: UICollectionViewFlowLayoutDelegate
    
    
    lazy var tovarCellSize = CGSize(width: 300, height: 300)
    lazy var headerCellSize = CGSize(width: 300, height: 300)
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            return headerCellSize
        case 1:
            return tovarCellSize
        default:
            return CGSize(width: 300, height: 300)
        }
    }
    
    lazy var headerSectionEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    lazy var tovarsSectionEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        switch section {
        case 1:
            return tovarsSectionEdgeInsets
        default:
            return headerSectionEdgeInsets
        }
    }
    
    
    override func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) { () -> Void in
            if self.товары.count == self.arrayMaxCount {
                if indexPath.row == self.товары.count - 1 {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.uploadMoreTovars()
                    }
                }
            }
        }
    }
    
    
    // MARK: - Товары
    
    
    var currentOffset = 0
    
    let quantityPerOneRequest = Constants.QuantityPerOneRequest
    
    var arrayMaxCount = Constants.QuantityPerOneRequest
    
    
    func uploadMoreTovars() {
        
        print("uploadTovar")
        
        currentOffset += quantityPerOneRequest
        
        arrayMaxCount += quantityPerOneRequest
        
        goods?.getTovaryFromGroupWithId(подборка!.ownerId, belongsToPodborkaWithId: подборка!.id, usingOffset: currentOffset, withMaxCount: quantityPerOneRequest, forViewController: self) { (товары, error) -> Void in }
    }
    
    
    // MARK: - VKGoods tovarny delegate
    
    
    var shouldLoad = true
    
    weak var viewController: UIViewController?
    
    
    func addNewTovar(товар: Товар) {
        
        if activityIndicatorView != nil {
            
            activityIndicatorView?.removeFromSuperview()
            activityIndicatorView = nil
        }

        товары.append(товар)
        
        collectionView?.insertItemsAtIndexPaths([NSIndexPath(forRow: товары.count - 1, inSection: 1)])
    }
    
    
    // MARK: - TovarsCollectionViewController protocol
    
    
    var selectedCellThumbPhotoTopIndent: CGFloat = 0
    var selectedCellThumbPhotoFrame = CGRect.zero
    var selectedCell: TovarCollectionViewCell?
    
    
    // MARK: - Navigation
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        guard segue.identifier == "Show Tovar" else { return }
        
        if let товар = sender as? Товар {
            goods?.loadPhotosOfTovar(товар)
            (segue.destinationViewController as? TovarTableViewController)?.товар = товар
        }
    }
    
    
    // MARK: - Notifications
    
    
    func updateCoverImageOfTovarFromNotification(notification: NSNotification) {
        
        guard   let товар = notification.object as? Товар,
                index = товары.indexOf({ $0 == товар }),
                cell = collectionView?.cellForItemAtIndexPath(NSIndexPath(forRow: index, inSection: 1)) as? TovarCollectionViewCell
                else { return }
        
        UIView.transitionWithView(cell.thumbImageView, duration: 0.25, options: .TransitionCrossDissolve, animations: { () -> Void in
            cell.thumbImageView.image = товар.thumbPhoto
            }, completion: nil)
    }
    
    
    func updateTovarFromNotification(notification: NSNotification) {
        
        guard let   товар = notification.object as? Товар,
                    index = товары.indexOf({ $0 == товар }) else { return }
        
        let indexPath = NSIndexPath(forRow: index, inSection: 1)
        
        guard collectionView?.cellForItemAtIndexPath(indexPath) != nil else { return }
        
        collectionView?.reloadItemsAtIndexPaths([indexPath])
    }
    
    
    func updateTovarLikeInCellFromNotification(notification: NSNotification) {
        
        guard   let cell = notification.object as? TovarCollectionViewCell,
                let indexPath = collectionView?.indexPathForCell(cell) where indexPath.row < товары.count,
                let likeIt = notification.userInfo?["likeIt"] as? Bool else { return }
        
        товары[indexPath.row].likeIt = likeIt
    }

}
