//
//  Category+CoreDataProperties.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/14/16.
//  Copyright © 2016 Beresta. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Category {

    @NSManaged var id: NSNumber?
    @NSManaged var name: String?
    @NSManaged var coverImageData: NSData?  
    @NSManaged var section: Section?

}
