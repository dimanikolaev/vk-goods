//
//  Protocols.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/21/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import Foundation


/**
     Этот протокол нужен для анимации переходов между вью контроллерами. У нас есть несколько вью контроллеров с UICollectionView с товарами внутри. Эти вью контроллеры являются экземплярами разных классов (PodborkaCollectionViewController, FavoritesCollectionViewController). Нам нужно, чтобы аниматор перехода (PushInAnimator) работал с экземплярами этих классов одинаково. 
     Поэтому мы добавляем протокол, в которм определяем те пропрети, которые должен поддерживать любой вью контроллер с UICollectionView с товарами внутри, чтобы PushInAnimator работал с этим вью контроллером корректно.
*/


protocol TovarsCollectionViewControllerProtocol {
    
    var selectedCellThumbPhotoTopIndent: CGFloat { get }
    var selectedCellThumbPhotoFrame: CGRect { get }
    var selectedCell: TovarCollectionViewCell? { get }
    
}

protocol PodborkasTableViewControllerProtocol {
    
    var selectedCellFrameIndent: CGFloat { get }
    var selectedCellFrame: CGRect { get}
    var selectedCell: PodborkaTableViewCellDefault? { get }
    var tableView: UITableView! { get set }
    
}


// MARK: - VKGoods delegates


protocol VKGoodsTovarnyDelegate: class {
    
    var shouldLoad: Bool { get }
    
    var viewController: UIViewController? { get }

    func addNewTovar(товар: Товар)
    
}

protocol VKGoodsPodborochnyDelegate: class {
    
    func addNewPodborka(подборка: Подборка)
    
}