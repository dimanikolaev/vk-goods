//
//  Constants.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/1/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import Foundation

class Constants {
    
    static let mainColor = UIColor(hue: 0.58, saturation: 0.68, brightness: 0.68, alpha: 1)
    
    static let PodborkaCellHeight: CGFloat = 192 //256
    
    static let CatalogPodborkaCellHeight: CGFloat = 128
    
    // MARK: - Chat
    
    static let ChatBackgroundColor = UIColor(white: 0.95, alpha: 1)
    
    static let SpeechBubbleCornersRadius: CGFloat = 18
    
    // MARK: - Navigation
    
    static let TransitionDuration: NSTimeInterval = 0.275
    static let TransitionOptions = UIViewAnimationOptions.CurveEaseInOut
    
    
    // MARK: API
    
    static let QuantityPerOneRequest: Int = 20
    
}