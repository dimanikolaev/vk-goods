//
//  PushInAnimator.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/20/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

class PushInAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    
    var transitionShouldBeCompleted = false
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return Constants.TransitionDuration
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        defer {
            if !transitionShouldBeCompleted {
                print("defer")
                transitionContext.completeTransition(false)
            }
        }
        
        guard   let containerView = transitionContext.containerView(),
                fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? TovarsCollectionViewControllerProtocol,
                fromView = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)?.view,
                toView = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)?.view else { return }
        
        transitionShouldBeCompleted = true
        
        let duration = transitionDuration(transitionContext)
        
        toView.frame = CGRect(origin: CGPoint(x: toView.frame.width, y: 0), size: toView.frame.size)
        
        toView.alpha = 0
        
        let maskRect = CGRect(x: 0, y: toView.frame.width, width: toView.frame.width, height: toView.frame.height - toView.frame.width)
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(rect: maskRect).CGPath

        toView.layer.mask = maskLayer
        
        containerView.addSubview(toView)
        
        let imageView = UIImageView(frame: fromViewController.selectedCellThumbPhotoFrame)
        imageView.image = fromViewController.selectedCell?.thumbImageView.image
        imageView.contentMode = .ScaleAspectFill
        imageView.clipsToBounds = true
        
        containerView.addSubview(imageView)
        
        fromViewController.selectedCell?.thumbImageView.hidden = true
        
        //        containerView.addSubview(tabBar)
        
        
        
        UIView.animateWithDuration(duration, delay: 0, options: Constants.TransitionOptions, animations: { () -> Void in
            
            fromView.frame = CGRect(origin: CGPoint(x: fromView.frame.width / -3, y: 0), size: fromView.frame.size)
            fromView.alpha = 0
            
            toView.frame = CGRect(origin: CGPoint.zero, size: toView.frame.size)
            toView.alpha = 1
            
            imageView.frame = CGRect(x: 0, y: fromViewController.selectedCellThumbPhotoTopIndent, width: toView.frame.width, height: toView.frame.width)
            
            }, completion: { (_) -> Void in
                
                fromView.alpha = 1
                
                toView.layer.mask = nil
                
                imageView.removeFromSuperview()
                
                fromViewController.selectedCell?.thumbImageView.hidden = false
                
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        })
        
    }
    
}
