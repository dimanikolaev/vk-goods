//
//  MagazinTableViewController.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/26/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit

class MagazinTableViewController: UITableViewController, PodborkasTableViewControllerProtocol {
    
    
    // MARK: - Properties
    
    
    let goods = (UIApplication.sharedApplication().delegate as? AppDelegate)?.goods
    
    var магазин: Magazin?
    
    
    // MARK: Table view
    
    
    let podborkaCellHeight = Constants.PodborkaCellHeight
    
    
    // MARK: Data
    
    
    var подборки = Array<Подборка>() {
        didSet {
            print(подборки)
            tableView.reloadSections(NSIndexSet(index: 1), withRowAnimation: .Fade)
        }
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateMagazFromNotification:", name: "magazHasBeenUpdated", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updatePodborkaFromNotification:", name: "podborkaHasBeenUpdated", object: nil)
        
        if let navigationBarFrame = navigationController?.navigationBar.frame {
            selectedCellFrameIndent = navigationBarFrame.origin.y + navigationBarFrame.height
        }
        
        guard магазин != nil else { return }
        
        navigationItem.title = магазин?.title
        
        goods?.getPodborkasOfGroupWithId(магазин!.id, inBackgroundWithBlock: { (подборки, error) -> Void in
            print(подборки)
            self.подборки = подборки
        })
    }
    
    
    override func viewDidAppear(animated: Bool) {
        if let navigationBarFrame = navigationController?.navigationBar.frame {
            navBarTopIndent = navigationBarFrame.origin.y + navigationBarFrame.height
        }
    }
    
    
    deinit {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    // MARK: - Table view delegate
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return podborkaCellHeight
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        guard indexPath.section != 0 else { return }
        
        selectedCell = tableView.cellForRowAtIndexPath(indexPath) as? PodborkaTableViewCellDefault
        
        performSegueWithIdentifier("Show Podborka", sender: indexPath)
    }
    
    
    var navBarTopIndent: CGFloat = 0
    
    var headerCell: MagazinHeaderTableViewCell?
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y <= -navBarTopIndent {
            headerCell?.frame = CGRect(x: 0, y: scrollView.contentOffset.y + navBarTopIndent, width: headerCell!.frame.width, height: podborkaCellHeight - scrollView.contentOffset.y - navBarTopIndent)
        }
    }
    
    // MARK: - Table view data source
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 3
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0: return 1
        case 1: return подборки.count
        case 2: return 1
        default: return 0
        }
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("Magazin Header Cell", forIndexPath: indexPath) as! MagazinHeaderTableViewCell
            cell.coverImageView.image = магазин?.photo
            
            headerCell = cell
            return cell
        
        case 1:
            let подборка = подборки[indexPath.row]
            
            let cell = tableView.dequeueReusableCellWithIdentifier("Podborka Default Cell", forIndexPath: indexPath) as! PodborkaTableViewCellDefault
            cell.coverImageView.image = подборка.coverImage
            cell.titleLabel.text = подборка.title
            
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier("Podborka Default Cell", forIndexPath: indexPath) as! PodborkaTableViewCellDefault
            cell.coverImageView.image = UIImage(named: "Marketplace")
            cell.titleLabel.text = "Все товары"
            
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    
    // MARK: - Podborkas table view controller protocol
    
    
    var selectedCellFrameIndent: CGFloat = 0
    var selectedCellFrame = CGRect.zero
    var selectedCell: PodborkaTableViewCellDefault?
    
    
    // MARK: - Navigation
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard let   podborkaViewController = segue.destinationViewController as? PodborkaCollectionViewController,
                    indexPath = sender as? NSIndexPath else { return }
        
        let rect = tableView.rectForRowAtIndexPath(sender as! NSIndexPath)
        
        selectedCellFrame = tableView.convertRect(CGRect(origin: CGPoint(x: 0, y: rect.origin.y - tableView.contentOffset.y - selectedCellFrameIndent), size: rect.size), fromView: view)
        
        if магазин != nil {
            podborkaViewController.ownerId = -магазин!.id
        }
        
        if indexPath.section == 1 && indexPath.row < подборки.count {
            podborkaViewController.подборка = подборки[indexPath.row]
        }
    }
    
    
    // MARK: - Notifications
    
    
    func updatePodborkaFromNotification(notification: NSNotification) {
        
        print("notification has been received")
        
        guard let   подборка = notification.object as? Подборка,
            index = подборки.indexOf({ $0 == подборка }),
            cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 1)) as? PodborkaTableViewCellDefault else { return }
        
        UIView.transitionWithView(cell.coverImageView, duration: 0.25, options: .TransitionCrossDissolve, animations: { () -> Void in
            cell.coverImageView.image = подборка.coverImage
            }, completion: nil)
    }
    
    
    func updateMagazFromNotification(notification: NSNotification) {
        
        guard let   магазин = notification.object as? Магазин else { return }
        
        if self.магазин == магазин {
            guard let coverImageView = (self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? MagazinHeaderTableViewCell)?.coverImageView else { return }
            UIView.transitionWithView(coverImageView, duration: 0.25, options: .TransitionCrossDissolve, animations: { () -> Void in
                coverImageView.image = магазин.photo
                }, completion: nil)
        }
    }
}
