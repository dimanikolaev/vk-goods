//
//  ProfileNavigationController.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/24/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit

class ProfileNavigationController: UINavigationController, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    let goods = (UIApplication.sharedApplication().delegate as? AppDelegate)?.goods
    
    var interactionController: UIPercentDrivenInteractiveTransition?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self

        let sreenEdgeGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: Selector("panned:"))
        sreenEdgeGestureRecognizer.edges = .Left
        sreenEdgeGestureRecognizer.delegate = self
        view.addGestureRecognizer(sreenEdgeGestureRecognizer)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        if let userId = NSUserDefaults.standardUserDefaults().valueForKey("userId") as? Int {
            goods?.updateAvaOfUserWithId(userId, block: { (data) -> Void in
                NSUserDefaults.standardUserDefaults().setValue(data, forKey: "userPhotoData")
            })
        }
        
        if viewControllers.count == 1 {
            guard let profileCollectionViewController = viewControllers.first as? ProfileCollectionViewController else { return }
            
            if profileCollectionViewController.shouldLoad {
                profileCollectionViewController.товары.removeAll()
                profileCollectionViewController.collectionView?.reloadData()
                
                profileCollectionViewController.shouldLoad = false
                
                goods?.getRecentlyVisitedTovarsInBackgroundWithBlock { (товары, error) -> Void in
                    profileCollectionViewController.shouldLoad = true
                    profileCollectionViewController.товары = товары

                }
            }
            
        }
    }
    
    
    // MARK: - Screen edge pan gesture recognizer
    
    func panned(recognizer: UIScreenEdgePanGestureRecognizer) {
        switch recognizer.state {
        case .Began:
            if viewControllers.count > 1 {
                interactionController = UIPercentDrivenInteractiveTransition()
                popViewControllerAnimated(true)
                
                interactionController?.finishInteractiveTransition()
                interactionController = nil
            }
        case .Changed:
            let translation = recognizer.translationInView(view)
            let progress = translation.x / view.bounds.width
            interactionController?.updateInteractiveTransition(progress)
        case .Ended:
            recognizer.velocityInView(view).x > 0 ? interactionController?.finishInteractiveTransition() : interactionController?.cancelInteractiveTransition()
            interactionController = nil
        default:
            interactionController?.cancelInteractiveTransition()
            interactionController = nil
        }
    }
    
    // MARK: - Screen edge pan gesture recognizer delegate
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    
    // MARK: - Navigation controller delegate
    
    
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        switch operation {
            
        case .Push:
            if fromVC is ProfileCollectionViewController {
                setNavigationBarHidden(false, animated: true)
                return PushInAnimator()
            }
            
            if toVC is CommentsViewController {
                return PushToChatAnimator()
            }
            
            return PushAnimator()
            
        case .Pop:
            if toVC is ProfileCollectionViewController {
                setNavigationBarHidden(true, animated: true)
                return PopAnimator()
            }
        
            if fromVC is CommentsViewController {
                return PopFromChatAnimator()
            }
            
            return PopAnimator()
            
        default:
            return nil
        }
    }
    
    
    func navigationController(navigationController: UINavigationController, interactionControllerForAnimationController animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        return interactionController
    }

}
