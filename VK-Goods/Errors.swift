//
//  Errors.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/26/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import Foundation


enum GetVKDataError: ErrorType {
    
    case HasNoItemsSection, RequestAPIError(additionalInfo: String)
    
    var description: String {
        switch self {
        case .HasNoItemsSection:
            return "Json ответ (`(response as VKResponse).json`) сервера не содержит элемента Items."
        default:
            return "Ошибка обращения к API. Дополнительная инфа в associated value of GetPodborkasError.RequestAPIError(String)."
        }
    }
}


enum GetDataFromBackendAppError: ErrorType {
    
    case HasNoResponseOrData, DataTaskError, JSONSerializationError, CanNotConvertJSONToNSArray
}