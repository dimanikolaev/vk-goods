//
//  TovarAddToBagButtonTableViewCell.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 3/12/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import UIKit

class TovarAddToBagButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var addToBagButton: UIButton! {
        didSet {
            addToBagButton.layer.cornerRadius = 26
            addToBagButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
            addToBagButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
            addToBagButton.setImage(UIImage(named: "Bag-1"), forState: .Normal)
            addToBagButton.setTitle("Добавить в корзину".localized.uppercaseString, forState: .Normal)
        }
    }
    
    @IBAction func tapOnAddToBagButton() {
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
