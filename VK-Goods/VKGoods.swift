//
//  VKGoods.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/18/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import VK_ios_sdk
import CoreData

/**
    `VKGoods` предоставляет методы для взаимодействия с серверным приложением Вконтакте (с помощью  VKSdk) и собственным бэкэнд приложением (которое необходимо для дополнительной функциональности).
*/

class VKGoods: NSObject, VKSdkDelegate, VKSdkUIDelegate {
    
    
    // MARK: Delegates
    
    
    weak var tovaryDelegate: VKGoodsTovarnyDelegate?
    weak var favoritesDelegate: VKGoodsTovarnyDelegate?
    weak var profileDelegate: VKGoodsTovarnyDelegate?
    
    weak var podborochnyDelegate: VKGoodsPodborochnyDelegate?
    
    
    // Адрес инстанса в vscale.ru, на котором бежит RESTful приложение на Спринге
    
    private let host = "https://delichonurbica.info:8443"
    
    private let permissions: Array<AnyObject> = [VK_PER_PHOTOS, VK_PER_GROUPS, VK_PER_MESSAGES, VK_PER_NOTIFICATIONS, VK_PER_STATUS, VK_PER_MARKET, VK_PER_WALL, VK_PER_FRIENDS]
    
    
    init(appId: String) {
        
        super.init()
        
        let sdkInstance = VKSdk.initializeWithAppId(appId)
        sdkInstance.registerDelegate(self)
        sdkInstance.uiDelegate = self
    }
    
    
    // MARK: - User
    
    
    func updateUserInfo(user: VKUser) {
        
        wakeUpSession {
            
            if user.first_name != nil && user.last_name != nil {
                NSUserDefaults.standardUserDefaults().setValue("\(user.first_name) \(user.last_name)", forKey: "userName")
            }
            
            if user.mobile_phone != nil {
                NSUserDefaults.standardUserDefaults().setValue(user.mobile_phone, forKey: "userMobilePhone")
            }
            
            if user.id != nil {
                NSUserDefaults.standardUserDefaults().setInteger(user.id as Int, forKey: "userId")
                
                // Подгружаем картинку большого разрешения из альбома аватарок
                
                self.updateAvaOfUserWithId(user.id as Int, block: { (data) -> Void in
                    NSUserDefaults.standardUserDefaults().setValue(data, forKey: "userPhotoData")
                })
            }
            
            if  user.photo_100 != nil {
                self.executeInBackground {
                    guard let   url = NSURL(string: user.photo_100),
                                data = NSData(contentsOfURL: url),
                                ava = UIImage(data: data),
                                circleAva = ava.circleImage else { return }
                    
                    NSUserDefaults.standardUserDefaults().setValue(UIImagePNGRepresentation(circleAva), forKey: "user100pxCirclePhotoData")
                }
            }
        }
    }
    
    
    func updateAvaOfUserWithId(id: Int, block: (data: NSData?) -> Void) {
        
        func setUserPhotoDataWithStringURL(stringURL: String) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) { () -> Void in
                guard let   url = NSURL(string: stringURL),
                            photoData = NSData(contentsOfURL: url),
                      var   userPhoto = UIImage(data: photoData) else {
                                block(data: nil)
                                return
                }
                
                // Если высота авы больше ширины, кропаем аву, чтобы получился квадрат
                if userPhoto.size.height > userPhoto.size.width {
                    let square = CGRect(x: 0, y: (userPhoto.size.height - userPhoto.size.width) / 2, width: userPhoto.size.width, height: userPhoto.size.width)
                    if let bitmapImage = CGImageCreateWithImageInRect(userPhoto.CGImage, square) {
                        userPhoto = UIImage(CGImage: bitmapImage)
                    }
                }
                
                dispatch_async(dispatch_get_main_queue()) {
                    block(data: UIImagePNGRepresentation(userPhoto))
                }
            }
        }
        
        wakeUpSession {
            
            let smallAvaRequest = VKRequest(method: "users.get", parameters: [VK_API_USER_ID: id, VK_API_FIELDS: "photo_100"])
            
            smallAvaRequest.executeWithResultBlock({ (response) -> Void in
                print(response)
                if let photo = ((response.json as? Array<AnyObject>)?.first as? Dictionary<String, AnyObject>)?["photo_100"] as? String {
                    self.executeInBackground {
                        guard let   url = NSURL(string: photo),
                                    data = NSData(contentsOfURL: url),
                                    ava = UIImage(data: data),
                                    circleAva = ava.circleImage else { return }
                        
                        NSUserDefaults.standardUserDefaults().setValue(UIImagePNGRepresentation(circleAva), forKey: "user100pxCirclePhotoData")
                    }
                }
                
                
                }) { (error) -> Void in
                    block(data: nil)
            }
            
            
            let request = VKRequest(method: "photos.get", parameters: [VK_API_USER_ID: id, VK_API_ALBUM_ID: "profile", "rev": 1, VK_API_COUNT: 1])
            
            request.executeWithResultBlock({ (response) -> Void in
                guard response.json != nil else {
                    block(data: nil)
                    return
                }
                guard let items = (response.json as? NSDictionary)?["items"] as? Array<AnyObject> where !items.isEmpty else {
                    block(data: nil)
                    return
                }
                
                if let photoStringURL = items[0]["photo_1280"] as? String {
                    setUserPhotoDataWithStringURL(photoStringURL)
                } else if let photoStringURL = items[0]["photo_807"] as? String {
                    setUserPhotoDataWithStringURL(photoStringURL)
                } else if let photoStringURL = items[0]["photo_604"] as? String {
                    setUserPhotoDataWithStringURL(photoStringURL)
                } else if let photoStringURL = items[0]["photo_130"] as? String {
                    setUserPhotoDataWithStringURL(photoStringURL)
                }
                }) { (error) -> Void in
                    block(data: nil)
                    print(error)
            }
        }
    }
    
    
    // MARK: - Категории
    
    
    func getCategoriesInBackground() {
        
        let catalogVersion = NSUserDefaults.standardUserDefaults().stringForKey("catalogVersion") ?? ""
        
        let request = NSMutableURLRequest(URL: NSURL(string: self.host + "/shouldUpdateCatalog?catalogVersion=\(catalogVersion)")!)
        request.HTTPShouldHandleCookies = false
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
            
            do {
                let arr = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as? Array<AnyObject>
                
                if let newVersion = arr?[1] as? String {
                    guard !newVersion.isEmpty else { return }
                    
                    
                    print("getCategories")
                    
                    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
                    
                    self.wakeUpSession {
                        
                        let request = VKRequest(method: "market.getCategories", parameters: [VK_API_COUNT: 1_000])
                        
                        request.executeWithResultBlock({ (response) -> Void in
                            
                            var categories = [(id: Int, name: String, section: (id: Int, name: String))]()
                            
                            var sectionsNames = [Int: String]()
                            var coreDataSections = [Int: Section]()
                            
                            if let items = response.json["items"] as? [Dictionary<String, AnyObject>] {
                                
                                for item in items {
                                    guard let   id = item["id"] as? Int,
                                        name = item["name"] as? String,
                                        sectionId = item["section"]?["id"] as? Int,
                                        sectionName = item["section"]?["name"] as? String else { break }
                                    
                                    categories.append((id: id, name: name, section: (id: sectionId, name: sectionName)))
                                    sectionsNames[sectionId] = sectionName
                                }
                                
                                let fetchRequest = NSFetchRequest(entityName: "Section")
                                fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
                                
                                do {
                                    let oldCoreDataSections = try managedObjectContext.executeFetchRequest(fetchRequest)
                                    
                                    for oldCoreDataSection in oldCoreDataSections as! [NSManagedObject] {
                                        // Категории секций также удаляются, так-как мы выбрали каскадное удаление
                                        managedObjectContext.deleteObject(oldCoreDataSection)
                                    }
                                    
                                    for section in sectionsNames {
                                        let coreDataSection = NSEntityDescription.insertNewObjectForEntityForName("Section", inManagedObjectContext: managedObjectContext) as! Section
                                        coreDataSection.id = section.0
                                        coreDataSection.name = section.1
                                        coreDataSections[coreDataSection.id as! Int] = coreDataSection
                                    }
                                    
                                    for category in categories {
                                        let coreDataCategory = NSEntityDescription.insertNewObjectForEntityForName("Category", inManagedObjectContext: managedObjectContext) as! Category
                                        coreDataCategory.id = category.id
                                        coreDataCategory.name = category.name
                                        coreDataCategory.section = coreDataSections[category.section.id]
                                    }
                                } catch {
                                    print("Core data error.")
                                }
                                
                                do {
                                    try managedObjectContext.save()
                                    NSUserDefaults.standardUserDefaults().setValue(newVersion, forKey: "catalogVersion")
                                } catch {
                                    print("Error: saving context error.")
                                }
                                
                            }
                            }) { (error) -> Void in
                                print(error)
                        }
                    }
                    
                    
                }
            } catch {
                print("Error: JSON serialization error.")
            }
        }
        
        task.resume()
        
    }
    
    
    // MARK: - Specials
    
    
    /**
        `getSpecialsInBackgroundWithBlock` запрашивает с сервера и возвращает в бэкграунде массив таплов с информацией о подборках, которые отображаются на экране «Популярное».
    */
    
    func getSpecialsInBackgroundWithBlock(specialsDate specialsDate: String = "", block: (specials: [(groupId: String, podborkaId: String, title: String)], error: GetDataFromBackendAppError?) -> Void) {
        
        wakeUpSession {
            
            let request = NSMutableURLRequest(URL: NSURL(string: self.host + "/specials?date=\(specialsDate)")!)
            request.HTTPShouldHandleCookies = false
            
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
                
                guard error == nil else {
                    print("Error: \(error!.debugDescription)")
                    block(specials: [], error: GetDataFromBackendAppError.DataTaskError)
                    return
                }
                
                guard response != nil && data != nil else {
                    print("Has no response or data.")
                    block(specials: [], error: GetDataFromBackendAppError.HasNoResponseOrData)
                    return
                }
                
                do {
                    let arr = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as? Array<AnyObject>
                    
                    guard arr?.count != 0 else {
                        
                        block(specials: [], error: GetDataFromBackendAppError.CanNotConvertJSONToNSArray)
                        return
                    }
                    
                    guard let   dic = arr?.first as? Dictionary<String, AnyObject>,
                                date = dic["date"] as? String,
                                specialsArr = dic["specials"] as? Array<AnyObject> else {
                                    
                                    block(specials: [], error: GetDataFromBackendAppError.CanNotConvertJSONToNSArray)
                                    return
                    }
                    
                    NSUserDefaults.standardUserDefaults().setValue(date, forKey: "specialsDate")
                    
                    var specials = Array<(groupId: String, podborkaId: String, title: String)>()
                    
                    for special in specialsArr {
                        guard let   groupId = special["groupId"] as? String,
                                    podborkaId = special["podborkaId"] as? String,
                                    title = special["title"] as? String
                        else {
                            continue
                        }
                        
                        specials.append((groupId: groupId, podborkaId: podborkaId, title: title))
                    }
                    
                    block(specials: specials, error: nil)
                    
                } catch {
                    print("Error: json serialization error.")
                    block(specials: [], error: GetDataFromBackendAppError.JSONSerializationError)
                }
            })
            
            task.resume()
        }
    }
    
    
    // MARK: - Магазины
    
    
    func getMagazinsForCategoryWithId(categoryId: Int, inBackgroundWithBlock block: (магазины: [Магазин], error: GetVKDataError?) -> Void) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: self.host + "/magazins?categoryId=\(categoryId)")!)
        request.HTTPShouldHandleCookies = false
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            
            guard error == nil else {
                print("Error: \(error!.debugDescription)")
                dispatch_async(dispatch_get_main_queue()) {
                    block(магазины: [], error:  GetVKDataError.RequestAPIError(additionalInfo: error!.debugDescription))
                }
                return
            }
            
            guard response != nil && data != nil else {
                print("Has no response or data.")
                dispatch_async(dispatch_get_main_queue()) {
                    block(магазины: [], error:  GetVKDataError.HasNoItemsSection)
                }
                return
            }
            
            var магазины = Array<Магазин>()
            
            do {
                let magazins = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as? Array<AnyObject>
                
                guard magazins?.count != 0 else {
                    dispatch_async(dispatch_get_main_queue()) {
                        block(магазины: [], error: nil)
                    }
                    return
                }
                
                var magazinsIds: String = ""
                
                magazins?.forEach {
                    if let magazinId = $0["vkID"] as? String {
                        magazinsIds += "\(magazinId),"
                    }
                }
                
                self.wakeUpSession {
                    
                    let request = VKRequest(method: "groups.getById", parameters: ["group_ids": magazinsIds, VK_API_FIELDS: "market", VK_API_EXTENDED: 1, ])
                    
                    request.executeWithResultBlock({ (response) -> Void in
                        
                        self.executeInBackground {
                            
                            guard let markets = response.json as? [Dictionary<String, AnyObject>] else {
                                dispatch_async(dispatch_get_main_queue()) {
                                    block(магазины: [], error:  GetVKDataError.HasNoItemsSection)
                                }
                                return
                            }
                            
                            for market in markets {
                                if let mag = Магазин(dic: market) {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        магазины.append(mag)
                                        self.getCoverOfMagazin(mag, inBackgroundWithBlock: { (cover) -> Void in
                                            mag.photo = cover
                                            NSNotificationCenter.defaultCenter().postNotificationName("magazHasBeenUpdated", object: mag)
                                        })
                                    })
                                }
                            }
                            
                            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                                block(магазины: магазины, error:  nil)
                            }
                        }
                        
                        }, errorBlock: { (error) -> Void in
                            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                                block(магазины: [], error:  GetVKDataError.RequestAPIError(additionalInfo: error.description))
                                print(error)
                            }
                    })
                }
                
                
            } catch {
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    block(магазины: [], error:  GetVKDataError.HasNoItemsSection)
                }
                print("Error: json serialization error.")
            }
            
        })
        
        task.resume()

    }
    
    
    func getCoverOfMagazin(магазин: Магазин, inBackgroundWithBlock block: (cover: UIImage?) -> Void) {
        
        func getImage(strURL: String) {
                
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) { () -> Void in
                guard let   url = NSURL(string: strURL),
                            photoData = NSData(contentsOfURL: url),
                            var   userPhoto = UIImage(data: photoData) else {
                                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                                    block(cover: nil)
                                }
                                return
                }
                
                // Если высота авы больше ширины, кропаем аву, чтобы получился квадрат
                if userPhoto.size.height > userPhoto.size.width {
                    let square = CGRect(x: 0, y: (userPhoto.size.height - userPhoto.size.width) / 2, width: userPhoto.size.width, height: userPhoto.size.width)
                    if let bitmapImage = CGImageCreateWithImageInRect(userPhoto.CGImage, square) {
                        userPhoto = UIImage(CGImage: bitmapImage)
                    }
                }
                
                print("done")
                
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    block(cover: userPhoto)
                }
            }
            
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                block(cover: nil)
            }
        }

        
        wakeUpSession {
            
            let request = VKRequest(method: "photos.get", parameters: [VK_API_OWNER_ID: -магазин.id, VK_API_ALBUM_ID: "profile", "rev": 1, VK_API_COUNT: 1])
            
            request.executeWithResultBlock({ (response) -> Void in
                guard response.json != nil else {
                    block(cover: nil)
                    return
                }
                guard let items = (response.json as? NSDictionary)?["items"] as? Array<AnyObject> where !items.isEmpty else {
                    block(cover: nil)
                    return
                }
                
                if let photoStringURL = items[0]["photo_1280"] as? String {
                    getImage(photoStringURL)
                } else if let photoStringURL = items[0]["photo_807"] as? String {
                    getImage(photoStringURL)
                } else if let photoStringURL = items[0]["photo_604"] as? String {
                    getImage(photoStringURL)
                } else if let photoStringURL = items[0]["photo_130"] as? String {
                    getImage(photoStringURL)
                }
                }) { (error) -> Void in
                    block(cover: nil)
                    print(error)
            }
        }

    }
    
    
    // MARK: - Подборки
    
    
    func getPodborkaInBackgroundWithId(podborkaId: String, fromGroupWithId groupId: String, withBlock block: (подборка: Подборка?, error: GetVKDataError?) -> Void) {
        
        wakeUpSession {
            
            let request = VKRequest(method: "market.getAlbumById", parameters: [VK_API_OWNER_ID: "-\(groupId)", "album_ids": podborkaId])
            
            request.executeWithResultBlock({ (response) -> Void in
                
                guard let items = response.json["items"] as? [Dictionary<String, AnyObject>] else {
                    block(подборка: nil, error: GetVKDataError.HasNoItemsSection)
                    return
                }
                
                if !items.isEmpty {
                    guard let podborka = Podborka(dic: items.first!) else {
                        block(подборка: nil, error: nil)
                        return
                    }
                    
                    block(подборка: podborka, error: nil)
                    
                } else {
                    block(подборка: nil, error: nil)
                }
                
                }, errorBlock: { (error) -> Void in
                    print(error)
                    block(подборка: nil, error: GetVKDataError.RequestAPIError(additionalInfo: error.description))
            })
        }
    }
    
    
    func getPodborkasOfGroupWithId(groupId: Int, usingOffset offset: UInt = 0, inBackgroundWithBlock block: (подборки: [Подборка], error: GetVKDataError?) -> Void) {
        
        wakeUpSession {
            
            let request = VKRequest(method: "market.getAlbums", parameters: [VK_API_OWNER_ID: "-\(groupId)", VK_API_OFFSET: offset])

            request.executeWithResultBlock({ (response) -> Void in
                
                var podborkas = [Podborka]()
                
                if let items = response.json["items"] as? [Dictionary<String, AnyObject>] {

                    self.executeInBackground {
        
                        for item in items {
                            if let podborka = Podborka(dic: item) { podborkas.append(podborka) }
                        }
                        
                        dispatch_async(dispatch_get_main_queue()) { () -> Void in
                            block(подборки: podborkas, error: nil)
                        }
                        
                    }
                    
                } else {
                    block(подборки: [], error: GetVKDataError.HasNoItemsSection)
                }
                }) { (error) -> Void in
                    block(подборки: [], error: GetVKDataError.RequestAPIError(additionalInfo: error.description))
            }
        }
    }
    
    
    // MARK: - Товары
    
    
    func getTovaryFromGroupWithId(groupId: Int, belongsToPodborkaWithId podborkaId: Int?, usingOffset offset: Int, withMaxCount maxCount: Int, forViewController viewController: UIViewController?, inBackgroundWithBlock block: (товары: [Товар], error: GetVKDataError?) -> Void) {
    
        var params: Dictionary<NSObject, AnyObject> = [VK_API_OWNER_ID: groupId, VK_API_OFFSET: offset < 0 ? 0 : offset, VK_API_COUNT: maxCount < 0 ? 50 : maxCount, VK_API_EXTENDED: 1]
        
        if podborkaId != nil {
            params[VK_API_ALBUM_ID] = podborkaId
        }
        
        let request = VKRequest(method: "market.get", parameters: params)
        
        request.executeWithResultBlock({ (response) -> Void in
            
            var tovary = [Товар]()
            
            if let items = response.json["items"] as? [Dictionary<String, AnyObject>] {
                
                self.executeInBackground {
                    
                    for item in items {
                        if let tovar = Tovar(dic: item) {
                            guard viewController == self.tovaryDelegate?.viewController else {
                                return }
                            
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                tovary.append(tovar)
                                self.tovaryDelegate?.addNewTovar(tovar)
                            })
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue()) { () -> Void in
                        block(товары: tovary, error: nil)
                    }
                }
            } else {
                block(товары: [], error: GetVKDataError.HasNoItemsSection)
            }
            }) { (error) -> Void in
                print(error)
                block(товары: [], error: GetVKDataError.RequestAPIError(additionalInfo: error.description))
        }
    }
    
    
    func getFavoriteTovarsUseOffset(offset: Int = 0, withMaxCount maxCount: Int, inBackgroundWithBlock block: (товары: [Товар], error: GetVKDataError?) -> Void) {
        
        wakeUpSession {
            
            let request = VKRequest(method: "fave.getMarketItems", parameters: ["offset": offset < 0 ? 0 : offset, VK_API_EXTENDED: 1, VK_API_COUNT: maxCount < 0 ? 50 : maxCount])
            
            request.executeWithResultBlock({ (response) -> Void in
                
                var tovary = [Товар]()
                
                if let items = response.json["items"] as? [Dictionary<String, AnyObject>] {
                    
                    self.executeInBackground {
                        
                        for item in items {
                            if let tovar = Tovar(dic: item) {
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    tovary.append(tovar)
                                    self.favoritesDelegate?.addNewTovar(tovar)
                                })
                            }
                        }
                        dispatch_async(dispatch_get_main_queue()) {
                            block(товары: tovary, error: nil)
                        }
                    }
                } else {
                    block(товары: [], error: GetVKDataError.HasNoItemsSection)
                }
                }, errorBlock: { (error) -> Void in
                    print(error)
                    block(товары: [], error: GetVKDataError.RequestAPIError(additionalInfo: error.description))
            })
        }
    }
    
    
    func addLikeToTovar(товар: Товар) {
        
        wakeUpSession {
            
            let request = VKRequest(method: "likes.add", parameters: ["type": "market", "item_id": товар.id, "owner_id": товар.ownerId])
            
            request.executeWithResultBlock({ (response) -> Void in
                }, errorBlock: { (error) -> Void in
                    print(error)
            })
        }
    }
    
    
    func takeLikeBack(товар: Товар) {
        
        wakeUpSession {
            
            let request = VKRequest(method: "likes.delete", parameters: ["type": "market", "item_id": товар.id, "owner_id": товар.ownerId])
            
            request.executeWithResultBlock({ (response) -> Void in
                }, errorBlock: { (error) -> Void in
                    print(error)
            })
        }
    }
    
    
    /**
     `loadPhotosOfTovar:` загружает фотографии товара и сохраняет их в массив `images`.
     */
    
    func loadPhotosOfTovar(товар: Товар) {
        
        wakeUpSession {
            
            let request = VKRequest(method: "market.getById", parameters: ["item_ids": "\(товар.ownerId)_\(товар.id)", VK_API_EXTENDED: 1])
            
            request.executeWithResultBlock({ (response) -> Void in
                
                guard let items = response.json["items"] as? [Dictionary<String, AnyObject>] where !items.isEmpty else { return }
                
                guard let photos = items.first!["photos"] as? [Dictionary<String, AnyObject>] where !photos.isEmpty else { return }
                
                
                var photosURLs = Array<NSURL>()
                
                for photo in photos {
                    var str = ""
                    if let photoURLString = photo["photo_1280"] as? String {
                        str = photoURLString
                    } else if let photoURLString = photo["photo_807"] as? String {
                        str = photoURLString
                    } else if let photoURLString = photo["photo_604"] as? String {
                        str = photoURLString
                    }
                    if let url = NSURL(string: str) {
                        photosURLs.append(url)
                    }
                }
                
                
                self.executeInBackground {
                    
                    for (index, url) in photosURLs.enumerate() {
                        guard let   data = NSData(contentsOfURL: url),
                                    photo = UIImage(data: data) else { continue }
                        
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            if товар.images.count > index {
                                товар.images[index] = photo
                            } else {
                                товар.images.append(photo)
                            }
                        })
                    }
                }
                
                }, errorBlock: { (error) -> Void in
                    print(error)
            })
        }
    }
    
    
    /**
     `addUserVisit:` обращается через серверное приложение к Монге, чтобы записать в `ArrayList` посещенных пользователем товаров новый товар.
     
     Последние посещенные пользователем товары отображаются на экране профиля.
     */
    
    func addUserVisit(товар товар: Товар) {
        
        guard let   userId = NSUserDefaults.standardUserDefaults().stringForKey("userId") else { return }
        
        let request = NSMutableURLRequest(URL: NSURL(string: self.host + "/addVKUserVisit?vkID=\(userId)&tovarID=\(товар.ownerId)_\(товар.id)")!)
        request.HTTPMethod = "POST"
        request.HTTPShouldHandleCookies = false
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            
            guard error == nil else {
                print("Error: \(error!.debugDescription)")
                return
            }
            
            guard response != nil && data != nil else {
                print("Has no response or data.")
                return
            }
            print(response)
        })
        
        task.resume()
    }
    
    
    /**
     `getRecentlyVisitedTovarsInBackgroundWithBlock:` возвращает массив последних просмотренных товаров.
     */
    
    func getRecentlyVisitedTovarsInBackgroundWithBlock(block: (товары: [Товар], error: GetVKDataError?) -> Void) {
        
        guard let userId = NSUserDefaults.standardUserDefaults().stringForKey("userId") else { return }
        
        let request = NSMutableURLRequest(URL: NSURL(string: self.host + "/vkUser?id=\(userId)")!)
        request.HTTPShouldHandleCookies = false
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            
            guard error == nil else {
                print("Error: \(error!.debugDescription)")
                return
            }
            
            guard response != nil && data != nil else {
                print("Has no response or data.")
                return
            }
            
            var tovary = Array<Товар>()
            
            do {
                let dic = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as? Dictionary<String, AnyObject>
                
                guard let visitedItems = dic?["visitedItems"] as? Array<String> else {
                    block(товары: [], error: nil)
                    return }
                
                var itemIds: String = ""
                
                visitedItems.forEach { itemIds += "\($0)," }
                
                self.wakeUpSession {
                    
                    let request = VKRequest(method: "market.getById", parameters: ["item_ids": itemIds, VK_API_EXTENDED: 1])
                    
                    request.executeWithResultBlock({ (response) -> Void in
                        
                        self.executeInBackground {
                            guard let items = response.json["items"] as? [Dictionary<String, AnyObject>] where !items.isEmpty else { return }
                            
                            for item in items {
                                if let tovar = Tovar(dic: item) {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        tovary.append(tovar)
                                        self.profileDelegate?.addNewTovar(tovar)
                                    })
                                }
                            }
                            
                            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                                block(товары: tovary, error: nil)
                            }
                        }
                        
                        }, errorBlock: { (error) -> Void in
                            print(error)
                    })
                }
                
                
            } catch {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    block(товары: [], error: nil)
                    print("Error: json serialization error.")
                })
            }
            
        })
        
        task.resume()
        
    }
    
    
    // MARK: - Comments
    
    
    func getCommentsToTovarWithId(tovarId: Int, belongsToOwnerWithId ownerId: Int, usingOffset offset: Int, withMaxCount maxCount: Int, inBackgroundWithBlock block: (comments: [Comment], error: GetVKDataError?) -> Void) {
        
        wakeUpSession {
            
            let params: Dictionary<NSObject, AnyObject> = ["item_id": tovarId, VK_API_OWNER_ID: ownerId, VK_API_OFFSET: offset < 0 ? 0 : offset, VK_API_COUNT: maxCount < 0 ? 50 : maxCount, VK_API_EXTENDED: 1,  VK_API_SORT: "desc"]
            
            let request = VKRequest(method: "market.getComments", parameters: params)
            
            request.executeWithResultBlock({ (response) -> Void in
                
                self.executeInBackground {
                    
                    var items = [Dictionary<String, AnyObject>]()
                    var profiles = [Dictionary<String, AnyObject>]()
                    var groups = [Dictionary<String, AnyObject>]()
                    
                    var avas = Dictionary<Int, UIImage>()
                    
                    if let itms = response.json["items"] as? [Dictionary<String, AnyObject>] { items = itms }
                    if let prfls = response.json["profiles"] as? [Dictionary<String, AnyObject>] { profiles = prfls }
                    if let grps = response.json["groups"] as? [Dictionary<String, AnyObject>] { groups = grps }
                    
                    var comments = Array<Comment>()
                    
                    for item in items {
                        guard let comment = Comment(dic: item) else { continue }
                        
                        guard avas[comment.id] == nil else {
                            comment.authorAva = avas[comment.id]!
                            continue
                        }
                        
                        comments.append(comment)
                        
                        var avaURLStr: String = ""
                        
                        if comment.authorId < 0 {
                            for group in groups {
                                if let groupId = group["id"] as? Int where -groupId == comment.authorId {
                                    if let avaStr = group["photo_100"] as? String {
                                        avaURLStr = avaStr
                                        break
                                    }
                                }
                            }
                        } else {
                            for profile in profiles {
                                if let profileId = profile["id"] as? Int where profileId == comment.authorId {
                                    if let avaStr = profile["photo_100"] as? String {
                                        avaURLStr = avaStr
                                        break
                                    }
                                }
                            }
                        }
                        
                        guard let   avaURL = NSURL(string: avaURLStr),
                            avaData = NSData(contentsOfURL: avaURL),
                            avaImage = UIImage(data: avaData) else { continue }
                        
                        comment.authorAva = avaImage.circleImage
                        avas[comment.id] = comment.authorAva
                    }
                    
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        block(comments: comments, error: nil)
                    }
                }
                
                }, errorBlock: { (error) -> Void in
                    print(error)
                    block(comments: [], error: GetVKDataError.RequestAPIError(additionalInfo: error.description))
            })
        }
    }
    
    
    func createCommentInGroupWithId(ownerId: Int, toTovarWithId tovarId: Int, withMessage message: String, withBlock block: (error: NSError?) -> Void) {
        
        wakeUpSession {
            
            let request = VKRequest(method: "market.createComment", parameters: [VK_API_OWNER_ID: ownerId, "item_id": tovarId, VK_API_MESSAGE: message])
            
            request.executeWithResultBlock({ (response) -> Void in
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    block(error: nil)
                }
                }, errorBlock: { (error) -> Void in
                    print(error)
                    block(error: error)
            })
        }
    }
    
    
    // MARK: - VKSdk Delegate
    
    
    func vkSdkAccessAuthorizationFinishedWithResult(result: VKAuthorizationResult!) {
        
        guard result.token != nil && result.user != nil else { return }
        
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "userIsAlreadyLoggedIn")
        
        updateUserInfo(result.user)
        
        getCategoriesInBackground()
    }
    
    
    func vkSdkUserAuthorizationFailed() {
        
        print("User authorization failed.")
    }
    
    
    // MARK: - VKSdk UI Delegate
    
    
    func vkSdkShouldPresentViewController(controller: UIViewController!) {
        
        (UIApplication.sharedApplication().delegate as? AppDelegate)?.window?.rootViewController?.presentViewController(controller, animated: true, completion: nil)
    }
    
    
    func vkSdkNeedCaptchaEnter(captchaError: VKError!) {
        
        print(captchaError)
    }
    
    
    // MARK: -
    
    
    func authorizeThroughVK() {
        
        VKSdk.authorize(permissions)
    }
    
    
    func logout() {
        
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "userIsAlreadyLoggedIn")
        
        let logInVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("LogIn View Controller") as! LogInViewController
        (UIApplication.sharedApplication().delegate as? AppDelegate)?.window?.rootViewController = logInVC
        
        VKSdk.forceLogout()
    }
    
    
    /**
        `wakeUpSession` поднимает сессию. Мы реализовываем это в виде отдельного метода, чтобы каждый раз не копировать один и тот же код с проверкой на `error` и `VKAuthorizationState`.
    */
    
    private func wakeUpSession(f: () -> Void) {

        VKSdk.wakeUpSession(permissions) { (state, error) -> Void in
            
            guard error == nil else {
                self.showAlert()
                return
            }
            
            switch state {
                
            case .Authorized: f()
                
            default:
                VKSdk.authorize(self.permissions)
            }
        }
    }
    
    
    private func executeInBackground(f: () -> Void) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) { () -> Void in
            f()
        }
    }
    
    
    /**
         `showAlert` показывает алерт на верхнем вью контроллере на нашем экране.
     */
    
    private func showAlert(title: String = "Что-то пошло не так 😥", message: String = "Попробуйте повторить попытку через некоторое время.") {
        
//        let alertView = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "OK")
//        
//        alertView.show()
        
        /*
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
        
        (UIApplication.sharedApplication().delegate as? AppDelegate)?.window?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
        */
    }
    
}
