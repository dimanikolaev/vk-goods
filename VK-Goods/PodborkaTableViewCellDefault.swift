//
//  PodborkaTableViewCellDefault.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/1/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//

import UIKit

class PodborkaTableViewCellDefault: UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet weak var coverImageView: UIImageView!
    
    @IBOutlet weak var titleView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}