//
//  PopAnimator.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/29/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//


class PopAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var transitionShouldBeCompleted = false
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return Constants.TransitionDuration
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        defer {
            if !transitionShouldBeCompleted {
                print("defer")
                transitionContext.completeTransition(false)
            }
        }
        
        guard   let containerView = transitionContext.containerView(),
                fromView = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)?.view,
                toView = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)?.view else { return }
        
        transitionShouldBeCompleted = true
        
        toView.frame = CGRect(origin: CGPoint(x: toView.frame.width / -3, y: 0), size: toView.frame.size)
        
        containerView.insertSubview(toView, belowSubview: fromView)
        
        fromView.clipsToBounds = false
        
        fromView.layer.shadowPath = UIBezierPath(rect: CGRect(origin: CGPoint(x: 16, y: 0), size: fromView.bounds.size)).CGPath

        fromView.layer.rasterizationScale = UIScreen.mainScreen().scale
        fromView.layer.shouldRasterize = true
        
        fromView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        fromView.layer.shadowOpacity = 1
        fromView.layer.shadowRadius = 16
        
        let duration = transitionDuration(transitionContext)
        
        UIView.animateWithDuration(duration, delay: 0, options: Constants.TransitionOptions, animations: { () -> Void in
            fromView.frame = CGRect(origin: CGPoint(x: fromView.frame.width, y: 0), size: fromView.frame.size)
            
            toView.frame = CGRect(origin: CGPoint.zero, size: toView.frame.size)

            }, completion: { (_) -> Void in 
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        })
    }
    
}

