//
//  CommentsViewController.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/26/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit

class CommentsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate {
    
    let goods = (UIApplication.sharedApplication().delegate as? AppDelegate)?.goods
    
    var товар: Товар?
    
    let userId = NSUserDefaults.standardUserDefaults().integerForKey("userId")
    
    var comments = Array<Comment>() {
        didSet {
            if let activityIndicatorView = (tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 1)) as? TovarCommentsTableViewCell)?.activityIndicatorView {
                activityIndicatorView.stopAnimating()
                activityIndicatorView.hidden = true
            }
        }
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.estimatedRowHeight = 41
            tableView.transform = CGAffineTransformMakeScale(1, -1)
        }
    }

    @IBOutlet weak var tableViewTopSpace: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewBottomSpace: NSLayoutConstraint!
    
    @IBOutlet weak var bottomSpace: NSLayoutConstraint!
    
    @IBOutlet weak var textView: UITextView! {
        didSet {
            textView.delegate = self
            textView.layer.cornerRadius = 6
            textView.backgroundColor = UIColor(white: 0.95, alpha: 1)
            textView.text = ""
        }
    }
    
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    
    var offset = 0
    var maxCount = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let tabBarHeight = tabBarController?.tabBar.frame.height, let navBarHeight = navigationController?.navigationBar.frame.height, navBarTopSpace = navigationController?.navigationBar.frame.origin.y {
            self.tabBarHeight = tabBarHeight
            bottomSpace.constant = -tabBarHeight + 6
            
            tableViewBottomSpace.constant = -navBarHeight - navBarTopSpace
            tableViewTopSpace.constant = navBarTopSpace + 6
        }
        
        guard товар != nil else { return }
        
        goods?.getCommentsToTovarWithId(товар!.id, belongsToOwnerWithId: товар!.ownerId, usingOffset: offset, withMaxCount: 10, inBackgroundWithBlock: { (comments, error) -> Void in
            print("Ok")
            print(comments)
            self.comments = comments
            self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Fade)
        })
    }
    
    
    override func viewDidAppear(animated: Bool) {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidShow:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    deinit {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0: return comments.count
        case 1: return 1
        default: return 0
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let comment = comments[indexPath.row]
            
            if comment.authorId == userId {
                let cell = tableView.dequeueReusableCellWithIdentifier("Right Cell", forIndexPath: indexPath) as! CommentTableViewCell
                cell.speechLabel?.text = comment.text
                cell.avaImageView.image = comment.authorAva
                return cell
            } else {
                let cell = tableView.dequeueReusableCellWithIdentifier("Left Cell", forIndexPath: indexPath) as! CommentTableViewCell
                cell.speechLabel?.text = comment.text
                cell.avaImageView.image = comment.authorAva
                return cell
            }
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("Tovar Cell", forIndexPath: indexPath) as! TovarCommentsTableViewCell
            cell.tovarImageView.image = товар?.thumbPhoto
            cell.tovarTitleLabel.text = товар?.title
            if comments.isEmpty { cell.activityIndicatorView.startAnimating() }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    // MARK: - Table view delegate
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) { () -> Void in
            if self.comments.count == self.maxCount {
                if indexPath.row == self.comments.count - 1 {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.uploadMoreComments()
                    }
                }
            }
        }
    }
    
    
    func uploadMoreComments() {
        
        offset += 10
        maxCount += 10
        
        if let activityIndicatorView = (tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 1)) as? TovarCommentsTableViewCell)?.activityIndicatorView {
            activityIndicatorView.startAnimating()
            activityIndicatorView.hidden = false
        }
        goods?.getCommentsToTovarWithId(товар!.id, belongsToOwnerWithId: товар!.ownerId, usingOffset: offset, withMaxCount: 10, inBackgroundWithBlock: { (comments, error) -> Void in
            self.comments += comments
            
            if let activityIndicatorView = (self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 1)) as? TovarCommentsTableViewCell)?.activityIndicatorView {
                activityIndicatorView.stopAnimating()
                activityIndicatorView.hidden = true
            }
            
            self.tableView.reloadData()
            
        })
    }
    
    
    // MARK: - Keyboard
    
    
    func keyboardWillShow(notification: NSNotification) {
        if let info: NSDictionary = notification.userInfo {
            if let value: NSValue = info.valueForKey(UIKeyboardFrameEndUserInfoKey) as? NSValue {
                if let keyboardSize: CGSize = value.CGRectValue().size as CGSize? {
                    bottomSpace.constant = keyboardSize.height - tabBarHeight + 6
                    view.layoutIfNeeded()
                }
            }
        }
    }
    
    
    func keyboardDidShow(notification: NSNotification) {
        tapGesture = UITapGestureRecognizer(target: self, action: Selector("tapGestureAction"))
        view.addGestureRecognizer(tapGesture!)
    }
    
    
    func keyboardWillHide(notification: NSNotification) {
        bottomSpace.constant =  -tabBarHeight + 6
        view.layoutIfNeeded()
    }
    
    
    var tapGesture: UITapGestureRecognizer?
    
    
    func tapGestureAction() {
        textView.endEditing(true)
        view.gestureRecognizers?.removeAll(keepCapacity: false)
        tapGesture = nil
    }
    
    
    var tabBarHeight: CGFloat = 0
    
    
    // MARK: - Text view
    
    
    func textViewDidBeginEditing(textView: UITextView) {
        if comments.count > 0 {
            tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: .Bottom, animated: false)
        }
    }
    
    
    func textViewDidChange(textView: UITextView) {
        
        print(textView.contentSize)

        let txtView = UITextView(frame: textView.frame)
        txtView.font = textView.font
        txtView.text = textView.text
        txtView.scrollEnabled = true
        txtView.sizeToFit()
        if textViewHeight.constant != txtView.frame.height && txtView.frame.height <= 158 {
            UIView.animateWithDuration(0.2, animations: { () -> Void in
                self.textViewHeight.constant = txtView.frame.height
                self.view.layoutIfNeeded()
                }, completion: { (completed) -> Void in
            })
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            if !textView.text.isEmpty {
                guard товар != nil else { return false }
                
                goods?.createCommentInGroupWithId(товар!.ownerId, toTovarWithId: товар!.id, withMessage: textView.text, withBlock: { (error) -> Void in
                    print(error)
                })
                
                if let comment = Comment(dic: ["id": -1, "from_id": userId, "text": textView.text]) {
                    
                    if let avaData = NSUserDefaults.standardUserDefaults().valueForKey("user100pxCirclePhotoData") as? NSData {
                        comment.authorAva = UIImage(data: avaData)
                    }
                    
                    comments.insert(comment, atIndex: 0)
                    tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: .Right)
                }
                
                textView.text = ""
            }
            return false
        }
        return true
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
