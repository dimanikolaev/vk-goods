//
//  PurchasesNavigationController.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 2/22/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit

class PurchasesNavigationController: UINavigationController, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    /*
     В Свифте есть модификатор `private`. Он позволяет скрыть класс из области видимости вне того файла, в котором этот класс определен. Мы могли бы просто написать `private class PushAnimator:...`. Но дело в том, что протокол `UIViewControllerAnimatedTransitioning` public. И класс, который  его реализует, тоже должен быть public. Поэтому, чтобы классы `PushAnimator` и `PopAnimator` находились в области видимости только внутри класса `PurchasesNavigationController`, мы определяем их внутри `PurchasesNavigationController`.
    */

    class PushAnimator: NSObject, UIViewControllerAnimatedTransitioning {
        
        var transitionShouldBeCompleted = false
        
        func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
            return Constants.TransitionDuration
        }
        
        func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
            defer {
                if !transitionShouldBeCompleted {
                    print("defer")
                    transitionContext.completeTransition(false)
                }
            }
            
            guard   let containerView = transitionContext.containerView(),
                    fromView = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)?.view,
                    toView = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)?.view,
                    tabBar = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)?.tabBarController?.tabBar
                    else { return }
            
            transitionShouldBeCompleted = true
            
            let duration = transitionDuration(transitionContext)
            
            toView.frame = CGRect(origin: CGPoint(x: toView.frame.width, y: 0), size: toView.frame.size)
            
            toView.layer.shadowColor = UIColor.lightGrayColor().CGColor
            toView.layer.shadowOpacity = 1
            toView.layer.shadowRadius = 6
            
            containerView.addSubview(toView)
            
            containerView.addSubview(tabBar)
            
            
            let animation = CABasicAnimation(keyPath: "shadowOffset.width")
            animation.fromValue = -1
            animation.toValue = 6
            animation.repeatCount = 1
            animation.duration = duration
            toView.layer.addAnimation(animation, forKey: "shadowOffset.width")
            
            UIView.animateWithDuration(duration, delay: 0, options: Constants.TransitionOptions, animations: { () -> Void in
                fromView.frame = CGRect(origin: CGPoint(x: fromView.frame.width / -3, y: 0), size: fromView.frame.size)
                
                toView.frame = CGRect(origin: CGPoint.zero, size: toView.frame.size)
                
                tabBar.frame = CGRect(origin: CGPoint(x: 0, y: fromView.frame.height), size: tabBar.frame.size)

                }, completion: { (_) -> Void in
                    transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
            })

        }
        
    }
    
    class PopAnimator: NSObject, UIViewControllerAnimatedTransitioning {
        
        var transitionShouldBeCompleted = false
        
        func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
            return Constants.TransitionDuration
        }
        
        func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
            defer {
                if !transitionShouldBeCompleted {
                    print("defer")
                    transitionContext.completeTransition(false)
                }
            }
            
            guard   let containerView = transitionContext.containerView(),
                    let fromView = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)?.view,
                    let toView = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)?.view,
                    let tabBar = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)?.tabBarController?.tabBar
                    else { return }
            
            transitionShouldBeCompleted = true
            
            toView.frame = CGRect(origin: CGPoint(x: toView.frame.width / -3, y: 0), size: toView.frame.size)
            
            containerView.insertSubview(toView, belowSubview: fromView)
            
            fromView.layer.shadowColor = UIColor.lightGrayColor().CGColor
            fromView.layer.shadowOpacity = 1
            fromView.layer.shadowRadius = 6
            
            containerView.addSubview(tabBar)
            
            let duration = transitionDuration(transitionContext)
            
            let animation = CABasicAnimation(keyPath: "shadowOffset.width")
            animation.fromValue = -1
            animation.toValue = 6
            animation.repeatCount = 1
            animation.duration = duration
            fromView.layer.addAnimation(animation, forKey: "shadowOffset.width")
            
            UIView.animateWithDuration(duration, delay: 0, options: Constants.TransitionOptions, animations: { () -> Void in
                fromView.frame = CGRect(origin: CGPoint(x: fromView.frame.width, y: 0), size: fromView.frame.size)
                
                toView.frame = CGRect(origin: CGPoint.zero, size: toView.frame.size)
                
                tabBar.frame = CGRect(origin: CGPoint(x: 0, y: fromView.frame.height - tabBar.frame.height), size: tabBar.frame.size)
                }, completion: { (_) -> Void in
                    ((UIApplication.sharedApplication().delegate as? AppDelegate)?.window?.rootViewController as? UITabBarController)?.view.addSubview(tabBar)
                    transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
            })
        }
        
    }
    
    var interactionController: UIPercentDrivenInteractiveTransition?

    override func viewDidLoad() {
        super.viewDidLoad()

        delegate = self
        
        let sreenEdgeGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: Selector("panned:"))
        sreenEdgeGestureRecognizer.edges = .Left
        sreenEdgeGestureRecognizer.delegate = self
        view.addGestureRecognizer(sreenEdgeGestureRecognizer)
    }
    
    // MARK: - Screen edge pan gesture recognizer
    
    func panned(recognizer: UIScreenEdgePanGestureRecognizer) {
        switch recognizer.state {
        case .Began:
            if viewControllers.count > 1 {
                interactionController = UIPercentDrivenInteractiveTransition()
                popViewControllerAnimated(true)
            }
        case .Changed:
            let translation = recognizer.translationInView(view)
            let progress = translation.x / view.bounds.width
            interactionController?.updateInteractiveTransition(progress)
        case .Ended:
            recognizer.velocityInView(view).x > 0 ? interactionController?.finishInteractiveTransition() : interactionController?.cancelInteractiveTransition()
            interactionController = nil
        default:
            interactionController?.cancelInteractiveTransition()
            interactionController = nil
        }
    }
    
    // MARK: - Screen edge pan gesture recognizer delegate
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    // MARK: - Navigation controller delegate
    
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch operation {
        case .Push:
            return PushAnimator()
        case .Pop:
            return PopAnimator()
        default:
            return nil
        }
    }
    
    func navigationController(navigationController: UINavigationController, interactionControllerForAnimationController animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactionController
    }
    
}


