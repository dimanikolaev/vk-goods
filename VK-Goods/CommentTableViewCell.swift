//
//  CommentTableViewCell.swift
//  VK-Goods
//
//  Created by Dima Nicholson on 3/27/16.
//  Copyright © 2016 Beresta. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
    var side = MessageSide.Left {
        didSet {
            switch side {
            case .Left:
                speechBubbleView.backgroundColor = UIColor(white: 0.95, alpha: 1)
                speechLabel.backgroundColor = UIColor(white: 0.95, alpha: 1)
            case .Right:
                speechBubbleView.backgroundColor = Constants.mainColor
                speechLabel.backgroundColor = Constants.mainColor
            }
        }
    }

    @IBOutlet weak var avaImageView: UIImageView! {
        didSet {
            avaImageView.layer.cornerRadius = 21
            avaImageView.layer.masksToBounds = false
            avaImageView.layer.shouldRasterize = true
            avaImageView.layer.rasterizationScale = UIScreen.mainScreen().scale
        }
    }
    
    @IBOutlet weak var speechBubbleView: UIView! {
        didSet {
            speechBubbleView.layer.cornerRadius = Constants.SpeechBubbleCornersRadius
            speechBubbleView.layer.masksToBounds = false
            speechBubbleView.layer.shouldRasterize = true
            speechBubbleView.layer.rasterizationScale = UIScreen.mainScreen().scale
        }
    }
    
    @IBOutlet weak var speechLabel: UILabel! {
        didSet {
            speechLabel.layer.masksToBounds = false
            speechLabel.layer.shouldRasterize = true
            speechLabel.layer.rasterizationScale = UIScreen.mainScreen().scale
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.transform = CGAffineTransformMakeScale(1, -1)
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
