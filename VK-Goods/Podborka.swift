//
//  Podborka.swift
//  VK-Shop
//
//  Created by Dima Nicholson on 2/29/16.
//  Copyright © 2016 Dima Nikolaev. All rights reserved.
//


typealias Podborka = Подборка

class Подборка: Equatable {
    
    let id: Int
    let ownerId: Int
    let title: String
    var coverImage: UIImage? = UIImage(named: "Marketplace")
    
    init?(dic: Dictionary<String, AnyObject>) {
        guard   let id = dic["id"] as? Int where id != 0,
                let title = dic["title"] as? String,
                    ownerId = dic["owner_id"] as? Int
                else {
                    self.id = 0
                    self.ownerId = 0
                    self.title = ""
                    return nil
        }
        
        self.id = id
        self.ownerId = ownerId
        self.title = title
        
        guard let photoDic = dic["photo"] as? Dictionary<String, AnyObject> else { return }
        
        var photoURLString = ""
        
        if let photoURLStr = photoDic["photo_1280"] as? String {
            photoURLString = photoURLStr
        } else if let photoURLStr = photoDic["photo_807"] as? String {
            photoURLString = photoURLStr
        } else if let photoURLStr = photoDic["photo_604"] as? String {
            photoURLString = photoURLStr
        } else if let photoURLStr = photoDic["photo_130"] as? String {
            photoURLString = photoURLStr
        }
        
        guard let photoURL = NSURL(string: photoURLString) else { return }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) { () -> Void in
            guard let imageData = NSData(contentsOfURL: photoURL) else { return }
            self.coverImage = UIImage(data: imageData)
            
            dispatch_async(dispatch_get_main_queue()) {
                print("notification has been pushed")
                NSNotificationCenter.defaultCenter().postNotificationName("podborkaHasBeenUpdated", object: self, userInfo: nil)
            }
            
        }
        
    }
    
}

// MARK: - Equatable protocol

func ==(lhs: Podborka, rhs: Podborka) -> Bool {
    return lhs.id == rhs.id && lhs.ownerId == rhs.ownerId
}